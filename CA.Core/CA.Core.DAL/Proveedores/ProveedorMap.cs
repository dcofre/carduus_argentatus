﻿using CA.Core.Domain.Proveedores.Entities;
using FluentNHibernate.Mapping;

namespace CA.Core.DBMaps.Proveedores
{
    public class ProveedorMap : ClassMap<Proveedor>
    {
        public ProveedorMap()
        {
            Table("PRV_Proveedor");

            Id(x => x.id)
                .GeneratedBy.Identity();

            Map(x => x.nombre)
                .Length(200);
        }
    }
}
