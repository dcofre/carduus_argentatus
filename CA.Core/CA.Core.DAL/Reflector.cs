﻿using System.Reflection;

namespace CA.Core.DBMaps
{
    public class Reflector
    {
        public static Assembly GetAssembly()
        {  
            return System.Reflection.Assembly.GetExecutingAssembly();   
        }
    }
}
