﻿namespace CA.Core.DBMaps.Bicicletas
{
    using Common.DBService.ClassMaps;
    using Domain.Bicicletas.Entities;
    using System;

    public class CotizacionMap : EntityMap<Cotizacion, int>
    {
        public CotizacionMap()
        {
            Table("SBI_Cotizacion");

            Id(x => x.id)
                .GeneratedBy.Identity();

            Map(x => x.nombre)
                .Length(255);

            Map(x => x.telefono)
                .Length(255);

            Map(x => x.email)
              .Length(255);

            Map(x => x.valor);

            Map(x => x.localidad)
                .Length(255);

            Map(x => x.source)
                .Length(255);

            Map(x => x.remember_token)
              .Length(255);
        }
    }
}
