﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CA.Core.WebApi;
using CA.Core.WebApi.Controllers;
using CA.Common.Base.Service.Log;

namespace CA.Core.WebApi.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            ILogger log = new Logger("Test");

            // Arrange
            HomeController controller = new HomeController(log);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
