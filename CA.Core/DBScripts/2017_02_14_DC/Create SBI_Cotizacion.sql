drop TABLE SBI_Cotizacion
CREATE TABLE SBI_Cotizacion (
  id int NOT NULL IDENTITY (1, 1),
  nombre varchar(255) NOT NULL,
  telefono varchar(255) NOT NULL,
  email varchar(255)  NOT NULL,
  valor money NOT NULL,
  localidad varchar(255) NOT NULL,
  source varchar(255) NOT NULL,
  remember_token varchar(100) NULL,
  entity_created_at datetime NOT NULL,
  entity_updated_at datetime NULL,  
  entity_deleted_at datetime NULL,
  entity_user varchar(50) NOT NULL
)