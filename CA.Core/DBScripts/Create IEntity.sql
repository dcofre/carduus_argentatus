CREATE TABLE IEntity (
  entity_created_at datetime NOT NULL,
  entity_updated_at datetime NULL,  
  entity_deleted_at datetime NULL,
  entity_user varchar(50) NOT NULL
)