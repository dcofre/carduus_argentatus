﻿using CA.Common.Base.Service.Log;
using CA.Common.Web.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace CA.Core.WebApi.Controllers
{
    public class HomeController : BaseController 
    {
        public HomeController(ILogger logger) : base(logger)
        {
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
