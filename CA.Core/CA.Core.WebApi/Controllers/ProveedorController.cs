﻿namespace CA.Core.WebApi.Controllers
{
    using CA.Core.API.DTO;
    using CA.Core.Domain.Proveedores.Entities;
    using CA.Common.DBService;
    using CA.Common.Web.WebApi;

    public class ProveedorController : GenericCRUDController<ProveedorDTO, Proveedor, int>
    {
        public ProveedorController(GenericCRUDService<ProveedorDTO, Proveedor, int> service) : base(service)
        {
        }
    }
}
