﻿namespace CA.Core.WebApi.Controllers
{
    using CA.Common.DBService;
    using CA.Core.API.DTO.Bicicletas;
    using CA.Core.Domain.Bicicletas.Entities;
    using Common.Web.WebApi;

    public class CotisBiciController : GenericCRUDController<CotizacionDTO, Cotizacion, int>
    {
        public CotisBiciController(GenericCRUDService<CotizacionDTO, Cotizacion, int> service) : base(service)
        {
        }
    }
}
