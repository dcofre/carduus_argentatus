﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Ninject;

[assembly: OwinStartup(typeof(CA.Core.WebApi.Startup))]

namespace CA.Core.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // hay que setear el assembly de donde se mapearan las entities antes de instanciar la uow
            CA.Common.DBService.NHbrnt.SessionFactory.MappingAssemblies.Add (CA.Core.DBMaps.Reflector.GetAssembly());
            CA.Common.DBService.NHbrnt.SessionFactory.MappingAssemblies.Add(CA.Common.Security.Reflector.GetAssembly());


            ConfigureAuth(app, NinjectWebCommon.Bootstrapper.Kernel);
        }
    }
}
