﻿using CA.Common.Base.Service.Log;
using CA.Common.IRepository;
using System;
using System.Net;
using System.Net.Http;

namespace CA.Core.WebApi.Infrastructure
{
    public class PerRequestScopeHandler : DelegatingHandler
    {
        public IUOW UoW { get; set; }

        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            UoW = GetUoW(request);
            if (UoW == null)
            {
                var statusCode = HttpStatusCode.InternalServerError;
                var responseMessage = request.CreateResponse(statusCode, "Error general procesando el request.");
                return System.Threading.Tasks.Task<HttpResponseMessage>.Factory.StartNew(() => responseMessage);
            }

            UoW.BeginTran();

            return base.SendAsync(request, cancellationToken).ContinueWith(t =>
            {
                UoW = GetUoW(request);
                if (UoW != null)
                {
                    UoW.CommitTran ();
                }
                return t.Result;
            });
        }

        private IUOW GetUoW(HttpRequestMessage request)
        {
            try
            {
                return request.GetDependencyScope().GetService(typeof(IUOW)) as IUOW;
            }
            catch (Exception ex)
            {
                var logger = request.GetDependencyScope().GetService(typeof(ILogger)) as ILogger;
                logger.Log(ex);
            }

            return null;
        }
    }
}