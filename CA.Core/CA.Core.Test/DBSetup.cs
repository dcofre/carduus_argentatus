﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Oracle.DataAccess.Client;
using System.Collections.Generic;

namespace SIR.PortalTramites.Test
{
    public static class DBSetup
    {
        static string _connectionString = "User Id=portaltramites;Password=Ab123456;Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.32.166)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=SIR)));";

        public static void prepararDBParaTest()
        {
            var oracleConnection = new OracleConnection(_connectionString);
            oracleConnection.Open();

            try
            {
                string[] cmdTexts = File.ReadAllLines("script.sql");

                List<string> cmdList = new List<string>();
                int currentCmd = 0;

                foreach (string cmdLine in cmdTexts)
                {
                    string tmpCmd = "";

                    if (cmdLine.Trim().Length > 0)
                    {
                        if (cmdLine.IndexOf("--") >= 0)
                        {
                            if (cmdLine.IndexOf("--") == 0)
                            {
                                tmpCmd = string.Empty;
                            }
                            else
                            {
                                tmpCmd = cmdLine.Substring(0, cmdLine.IndexOf("--"));
                            }
                        }
                        else
                        {
                            tmpCmd = cmdLine;
                        }

                        if (tmpCmd != string.Empty)
                        {
                            if (currentCmd == cmdList.Count)
                            {
                                cmdList.Add("");
                            }

                            if (tmpCmd.EndsWith(";"))
                            {
                                cmdList[currentCmd++] += " " + tmpCmd.Substring(0, cmdLine.Length - 1);
                            }
                            else
                            {
                                cmdList[currentCmd] += " " + tmpCmd;
                            }
                        }
                    }
                }

                for (int posicion = 0; posicion < cmdList.Count; posicion++)
                {
                    while (cmdList[posicion].IndexOf("  ") >= 0)
                    {
                        cmdList[posicion] = cmdList[posicion].Replace("  ", " ");
                    }


                }

                foreach (string cmd in cmdList)
                {
                    OracleCommand command = new OracleCommand(cmd, oracleConnection);
                    command.CommandType = System.Data.CommandType.Text;
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                oracleConnection.Close();
                oracleConnection.Dispose();
            }
        }
    }
}
