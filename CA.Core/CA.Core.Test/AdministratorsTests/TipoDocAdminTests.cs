﻿using CA.Core.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA.Core.Test.AdministratorsTests
{
    [TestClass]
    public class TipoDocAdminTests : GenericDBAdminTests<TipoDocumento>
    {
        public override object GetTestEntityId()
        {
            return new Guid("77b886cf-a6a3-4fd1-94c0-0f97fbf6bd57");
        }

        public override TipoDocumento GetTestEntity()
        {
            TipoDocumento ent = base.GetTestEntity();
            ent.Id = (Guid)this.GetTestEntityId();
            ent.Descripcion = "Test Description";
            ent.Codigo = "TEST";
            ent.Regex = "TEST";
            return ent;
        }


        [TestMethod]
        public override void CanCRUD()
        {
            base.CanCRUD();
        }
    }
}
