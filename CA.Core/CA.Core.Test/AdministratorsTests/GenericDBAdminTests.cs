﻿namespace CA.Core.Test
{
    using Common.Base.Service;
    using Common.Base.Service.Log;
    using Common.Base.ServiceInterfaces;
    using Common.DBService;
    using Common.DBService.DBEntityAdministrators;
    using Common.DBService.Repository;
    using Common.Entities;
    using Common.IRepository;
    using Domain.Administrators;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    public abstract class GenericDBAdminTests<TEntity> where TEntity : IEntity
    {
        public GenericDBAdminTests()
        {
            this.Logger = new Logger("CA.Core.Test");
            this.Pars = new SvcParams();
            this.Pars.DebugMode = true;

            CA.Common.DBService.NHbrnt.SessionFactory.ShowSql = true;
            // hay que setear el assembly de donde se mapearan las entities antes de instanciar la uow
            CA.Common.DBService.NHbrnt.SessionFactory.MappingAssembly = CA.Core.DAL.Reflector.GetAssembly();

            //DOMAIN 
            this.Uow = new UOW(Logger);
            this.Config = new ConfigService(this.Pars, this.Logger, this.Uow);
            this.UserAdmin = new UserAdmin(this.Pars, this.Logger, this.Uow, this.Config);
        }

        protected IUOW Uow { get; private set; }
        protected ILogger Logger { get; private set; }
        protected IConfigService Config { get; private set; }
        protected ISvcParams Pars { get; private set; }
        protected IUserAdmin UserAdmin { get; private set; }

        private GenericDBAdministrator<TEntity> _entityAdmin;
        protected GenericDBAdministrator<TEntity> EntityAdmin
        {
            get
            {
                if (_entityAdmin == null)
                {
                    _entityAdmin = this.EntityAdminFactory();
                }

                return _entityAdmin;
            }
        }

        protected virtual GenericDBAdministrator<TEntity> EntityAdminFactory()
        {
            return new GenericDBAdministrator<TEntity>(this.Pars, this.Logger, this.Uow, this.Config);
        }

        public virtual void CanCRUD()
        {
            this.Uow.BeginTran();
            var entity = UpsertTestEntity();

            this.Uow.CommitTran();

            Assert.IsTrue(this.CanGetTestEntity());

            this.EntityAdmin.Delete(entity);

            Assert.IsFalse(this.CanGetTestEntity());

            this.Uow.CommitTran();
        }

        public virtual bool CanGetTestEntity()
        {
            return (this.EntityAdmin.Get(this.GetTestEntityId()) != null);
        }

        public virtual TEntity UpsertTestEntity()
        {
            return this.EntityAdmin.Save(this.GetTestEntity());
        }

        public virtual TEntity GetTestEntity()
        {
            TEntity ent = this.EntityAdmin.Create();

            return ent;
        }

        public abstract object GetTestEntityId();
    }
}