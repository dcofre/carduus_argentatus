﻿using AutoMapper;
using CA.Core.API.DTO;
using CA.Core.API.DTO.Bicicletas;
using CA.Core.Domain.Bicicletas.Entities;
using CA.Core.Domain.Proveedores.Entities;


namespace CA.Core.API.Config
{
    public static class AutoMapperConfig
    {
        public static IMapper BuildMapper()
        {
            return new Mapper(AutoMapperConfig.CreateMaps());
        } 

        private static MapperConfiguration CreateMaps()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Proveedor, ProveedorDTO>();
                cfg.CreateMap<ProveedorDTO, Proveedor>();
                cfg.CreateMap<CotizacionDTO, Cotizacion>().ReverseMap();
            });

            return config;

            //AutoMapper.Mapper.CreateMap<PersonDTO, Person>()
            //AutoMapper.Mapper.Instance.ConfigurationProvider.CreateMapper(.CreateMapper<Proveedor,ProveedorDTO>()   ;
            //    .ForMember(dest => dest.Author,
            //               opts => opts.MapFrom(src => src.Author.Name));
        }
    }
}