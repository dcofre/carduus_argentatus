﻿
using AutoMapper;
using CA.Common.Base.Service;
using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.DBService;
using CA.Common.DBService.Repository;
using CA.Common.IRepository;
using CA.Common.Security.Administrators;
using CA.Common.Web.Authorization;
using CA.Core.API.Services;
using CA.Core.Domain.Proveedores.Administrators;
using Microsoft.Owin.Security.OAuth;
using Ninject.Modules;

namespace CA.Core.API.Config
{
    public class NinjectConfig : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ILogger>().To<Logger>()
                   .WithConstructorArgument("loggerName", "CA.Core");

            var debug = false;
#if DEBUG
            debug = true;
#endif

            Kernel.Bind<ISvcParams>().To<SvcParams>().WithConstructorArgument("debugMode", debug);

            Kernel.Bind<IMapper>().ToConstant(AutoMapperConfig.BuildMapper());


            Kernel.Bind<IOAuthAuthorizationServerProvider>().To<OAuthProvider>().WithConstructorArgument<string>("ClientId");

            //DOMAIN                       .
            Kernel.Bind<IUOW>().To<UOW>();
            Kernel.Bind<IConfigService>().To<ConfigService>();
            Kernel.Bind<IUsuarioAdmin>().To<UsuarioAdmin>();
            Kernel.Bind<IProveedorAdmin>().To<ProveedorAdmin>();


            //API
            Kernel.Bind<IProveedorService>().To<ProveedorService>();
        }

    }
}