﻿using CA.Core.API.DTO;

namespace CA.Core.API.Services
{
    public interface IProveedorService
    {
        ProveedorDTO GetByNombre(string nombre);
    }
}