﻿namespace CA.Core.API.Services
{
    using AutoMapper;
    using CA.Common.Base.Service.Log;
    using CA.Common.Base.ServiceInterfaces;
    using CA.Core.API.DTO;
    using Common.DBService;
    using Domain.Proveedores.Entities;
    using Common.IRepository;
    using Domain.Proveedores.Administrators;

    public class ProveedorService : GenericCRUDService<ProveedorDTO, Proveedor, int>, IProveedorService
    {
        public ProveedorService(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config, IMapper mapper) : base(pars, logger, uow, config, mapper)
        { }

        protected ProveedorAdmin ProveedorAdmin { get; }

        public ProveedorDTO GetByNombre(string nombre)
        {
            return this.Mapper.Map<Proveedor, ProveedorDTO>(this.ProveedorAdmin.GetByNombre(nombre));
        }
    }
}

