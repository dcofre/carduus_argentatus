﻿namespace CA.Core.API.Services.Bicicletas
{
    using AutoMapper;
    using CA.Common.Base.Service.Log;
    using CA.Common.Base.ServiceInterfaces;
    using Common.DBService;
    using Common.IRepository;
    using Domain.Bicicletas.Entities;
    using DTO.Bicicletas;

    public class CotizacionService : GenericCRUDService<CotizacionDTO, Cotizacion, int>, IGenericCRUDService<CotizacionDTO, Cotizacion, int>
    {
        public CotizacionService(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config, IMapper mapper) : base(pars, logger, uow, config, mapper)
        { }

        //protected ProveedorAdmin ProveedorAdmin { get; }

        //public ProveedorDTO GetByNombre(string nombre)
        //{
        //    return this.Mapper.Map<Proveedor, ProveedorDTO>(this.ProveedorAdmin.GetByNombre(nombre));
        //}
    }
}

