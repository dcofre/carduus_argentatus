﻿using CA.Common.DTOs;
using System;

namespace CA.Core.API.DTO
{
    public class ProveedorDTO : BaseDTO
    {
        public Guid ID { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Regex { get; set; }
        public string Formato { get; set; }
    }
}
