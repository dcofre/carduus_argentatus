﻿namespace CA.Core.API.DTO.Bicicletas
{
    using CA.Common.DTOs;

    public class CotizacionDTO : BaseDTO
    {
        public virtual int id { get; set; }
        public virtual string nombre { get; set; }
        public virtual string telefono { get; set; }
        public virtual string email { get; set; }
        public virtual decimal valor { get; set; }
        public virtual string localidad { get; set; }
        public virtual string source { get; set; }
    }
}
