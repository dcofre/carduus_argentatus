﻿namespace CA.Core.Domain.Proveedores.Entities
{
    using CA.Common.Entities;

    public class Proveedor : BaseEntity <int>
    {
        public Proveedor() : base()
        {
        }

        public virtual string nombre { get; set; }

        public override bool Validate()
        {
            //Todo Validaciones de Entidad
            return true;
        }
    }
}