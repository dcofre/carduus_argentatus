﻿using Management;
using SIR.Common.Service.Log;
using SIR.PagoElectronico.Connectors;
using System;

namespace SIR.Core.Domain.Services
{
    public class CodBarraService : BaseService, ICodBarraService
    {
        private readonly IUOW _uoW;

        public CodBarraService(IUOW uow, ILogger logger)
            : base(logger)
        {
            if (uow == null)
            {
                throw new ArgumentNullException("uow");
            }

            // SGCBA-6330: Se elimina sistema de licenciamiento.
            //
            //var val = new Validate
            //{
            //    ph = SerialKeyConfiguration.lE7BH3J23(),
            //    Key = uow.ConfigRepository.FindById("Hash").Value
            //};
            //if (val.IsValid && !val.IsExpired && val.SetTime >= val.DaysLeft)
            //{
            //    _uoW = uow;
            //}
            //else
            //{
            //    throw new Exception("Licencia vencida");
            //}

            _uoW = uow;
        }

        public decimal GetImportePagoVoluntario(string codigoBarra)
        {
            var urlCordigoBarra = _uoW.ConfigRepository.FindById("sirCodigoBarra_Url").Value;
            var svc = new CodigoBarraClient(urlCordigoBarra, logger);

            return svc.GetImportePagoVoluntario(codigoBarra);
        }
    }
}