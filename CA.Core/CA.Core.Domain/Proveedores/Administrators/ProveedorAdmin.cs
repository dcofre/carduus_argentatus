﻿using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.DBService.EntityAdministrators;
using CA.Common.Exceptions;
using CA.Common.IRepository;
using CA.Core.Domain.Proveedores.Entities;
using System;
using System.Linq;

namespace CA.Core.Domain.Proveedores.Administrators
{
    public class ProveedorAdmin : GenericEntityAdministrator<Proveedor, int>, IProveedorAdmin
    {
        public ProveedorAdmin(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config) : base(pars, logger, uow, config)
        { }

        public Proveedor GetByNombre(string nombre)
        {
            try
            {
                var entity = this.UoW.RepoCache<Proveedor>().Find(x => x.nombre.ToLower() == nombre.TrimEnd().ToLower()).FirstOrDefault();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ServiceException("No fue posible obtener el tipo de documento.", ex);
            }
        }

    }
}
