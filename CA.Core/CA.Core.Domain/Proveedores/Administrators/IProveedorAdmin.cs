﻿using CA.Core.Domain.Proveedores.Entities;

namespace CA.Core.Domain.Proveedores.Administrators
{
    public interface IProveedorAdmin
    {
        Proveedor GetByNombre(string nombre);
    }
}