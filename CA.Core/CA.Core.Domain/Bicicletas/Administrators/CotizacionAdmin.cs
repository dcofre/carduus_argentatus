﻿namespace CA.Core.Domain.Bicicletas.Administrators
{
    using Common.DBService.EntityAdministrators;
    using Entities;
    using System;
    using Common.Base.Service.Log;
    using Common.Base.ServiceInterfaces;
    using Common.IRepository;

    public class CotizacionAdmin : GenericEntityAdministrator<Cotizacion, int>
    {
        public CotizacionAdmin(ISvcParams flags, ILogger logger, IUOW uow, IConfigService config) : base(flags, logger, uow, config)
        {
        }
    }
}
