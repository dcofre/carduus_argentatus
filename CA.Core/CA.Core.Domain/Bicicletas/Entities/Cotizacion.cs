﻿namespace CA.Core.Domain.Bicicletas.Entities
{
    using Common.Entities;
    using System;

    public class Cotizacion : BaseEntity<int>
    {
        public virtual string nombre { get; set; }
        public virtual string telefono { get; set; }
        public virtual string email { get; set; }
        public virtual decimal valor { get; set; }
        public virtual string localidad { get; set; }
        public virtual string source { get; set; }
        public virtual string remember_token { get; set; }

        public override bool Validate()
        {
            //Todo Validaciones de Entidad
            return true;
        }
    }
}
