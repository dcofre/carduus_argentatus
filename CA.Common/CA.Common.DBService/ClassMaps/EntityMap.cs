﻿namespace CA.Common.DBService.ClassMaps
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class EntityMap<TEntity, TId> : ClassMap<TEntity>
        where TEntity : IEntity<TId>
    {
        public EntityMap()
        {
            Map(x => x.entity_created_at);
            Map(x => x.entity_updated_at);
            Map(x => x.entity_deleted_at);
            Map(x => x.entity_user);
        }
    }
}
