﻿using FluentNHibernate.Mapping;

namespace CA.Common.ClassMaps
{
    public class ConfigMap : ClassMap<Entities.Config>
    {
        public ConfigMap()
        {
            Table("COMON_CONFIG");

            Id(x => x.id)
                .GeneratedBy.Identity();

            Map(x => x.ModuleId)
                .Length(10)
                .Not.Nullable();

            Map(x => x.Key)
                .Column ("_Key")
                .Length(40)
                .Not.Nullable();

            Map(x => x.Debug)
                .Not.Nullable();

            Map(x => x.Value)
                .Length(2000)
                .Not.Nullable();

            Map(x => x.Description)
                .Length(2000)
                .Nullable();
        }
    }
}
