﻿using AutoMapper;
using CA.Common.Base.Service;
using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.IRepository;
using System;

namespace CA.Common.DBService
{
    public class BaseDBService : BaseService, IDBService
    {
        public BaseDBService(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config, IMapper mapper) : this(pars, logger, uow, config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("mapper");
            }
            this.Mapper  = mapper ;
        }

        public BaseDBService(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config) : this(pars, logger, uow)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            this.Config = config;
        }

        public BaseDBService(ISvcParams pars, ILogger logger, IUOW uow) : this(pars, logger)
        {
            if (uow == null)
            {
                throw new ArgumentNullException("uow");
            }
            this.UoW = uow;
        }

        public BaseDBService(ISvcParams pars, ILogger logger) : base(pars, logger)
        {
        }

        public IUOW UoW { get; set; }
        public IConfigService Config { get; set; }
        public IMapper Mapper { get; set; }

    }
}
