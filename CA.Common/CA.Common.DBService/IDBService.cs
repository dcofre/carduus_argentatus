﻿namespace CA.Common.DBService
{
    using CA.Common.Base.ServiceInterfaces;
    using CA.Common.IRepository;

    public interface IDBService
    {
        IConfigService Config { get; set; }
        IUOW UoW { get; set; }
    }
}