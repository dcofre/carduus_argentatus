﻿namespace CA.Common.DBService
{
    using AutoMapper;
    using CA.Common.Base.Service.Log;
    using CA.Common.Base.ServiceInterfaces;
    using CA.Common.DTOs;
    using CA.Common.Entities;
    using CA.Common.IRepository;
    using DBService;
    using EntityAdministrators;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class GenericCRUDService<TDTO, TEntity, TId> : BaseDBService, IGenericCRUDService<TDTO, TEntity,TId>
    where TDTO : IDTO
    where TEntity : IEntity<TId>
    {
        public GenericCRUDService(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config, IMapper mapper) : base(pars, logger, uow, config, mapper)
        {
            this.Admin = new GenericEntityAdministrator<TEntity, TId>(this.Pars, this.Logger, this.UoW, this.Config);
        }

        protected GenericEntityAdministrator<TEntity, TId> Admin { get; set; }

        /// <summary>
        /// Obtiene un DTO por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TDTO Read(TId id)
        {
            TDTO retVal = default(TDTO);
            try
            {
                var entity = this.Admin.GetOne(id);
                retVal = this.Mapper.Map<TDTO>(entity);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Obtiene listado de tipoDocumentos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TDTO> ReadAll()
        {
            try
            {
                var entList = this.Admin.GetAll();
                return entList.Select(e => Mapper.Map<TDTO>(e)).ToArray();
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
                throw;
            }
        }

        /// <summary>
        /// Persiste el dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public TDTO Create(TDTO dto)
        {
            TDTO retVal = default(TDTO);
            try
            {
                TEntity ent = this.Admin.Save(Mapper.Map<TDTO, TEntity>(dto));
                retVal = this.Mapper.Map<TEntity, TDTO>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Persiste el dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public TDTO Update(TId id, TDTO dto)
        {
            TDTO res = default(TDTO);
            try
            {
                var ent = this.Admin.Update(id, this.Mapper.Map<TDTO, TEntity>(dto));
                res = this.Mapper.Map<TEntity, TDTO>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return res;
        }

        /// <summary>
        /// Persiste el dto
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void Delete(TId id)
        {
            try
            {
                this.Admin.Delete(id);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
        }


    }
}
