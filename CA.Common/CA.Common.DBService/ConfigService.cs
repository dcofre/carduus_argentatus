﻿using System;
using System.Collections.Generic;
using System.Linq;
using CA.Common.DTOs;
using CA.Common.IRepository;
using CA.Common.Base.Service.Log;
using CA.Common.Exceptions;
using CA.Common.Base.ServiceInterfaces;

namespace CA.Common.DBService
{
    public class ConfigService : BaseDBService, IConfigService
    {
        public ConfigService(ISvcParams flags, ILogger logger, IUOW uow) : base(flags, logger, uow)
        { }

        public virtual string ModuleId
        {
            get
            {
                return "BASE";
            }
        }

        public ConfigDTO Get(string key)
        {
            return Get(key, ModuleId);
        }

        public ConfigDTO Get(string key, string moduleId)
        {
            ConfigDTO retVal = null;

            if (Pars.DebugMode)
            {
                //override para dev local
                retVal = GetFromDB(key, moduleId, true);
                if (retVal != null) return retVal;
            }

            return GetFromDB(key, moduleId, false);
        }

        private ConfigDTO GetFromDB(string key, string moduleId, bool debug)
        {
            ConfigDTO retVal = null;
            try
            {
                var entity = this.UoW.RepoCache<Entities.Config>().Find(c => c.ModuleId == moduleId && c.Key == key && c.Debug == debug).SingleOrDefault();
                if (entity != null)
                {
                    retVal = (ConfigDTO)entity.Map();
                    Logger.Debug(string.Format("Config: Get ModuleId '{0}' Key '{1}' OK", moduleId, key));
                }
                else
                {
                    Logger.Warn(string.Format("Config: ModuleId '{0}' Key '{1}' NOT FOUND!", moduleId, key));
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ServiceException(string.Format("Couldn't get key {0} module {1} from config repository", key, moduleId), ex);
            }

            return retVal;
        }

        public ConfigDTO Upsert(ConfigDTO dto)
        {
            ConfigDTO retVal = null;
            try
            {
                dto.ModuleId = this.ModuleId;
                var entity = UoW.RepoCache<Entities.Config>().SaveOrUpdate(new Entities.Config());
                retVal = entity.Map() as ConfigDTO;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ServiceException("Couldn't insert entity", ex);
            }

            return retVal;
        }

        public IDictionary<string, ConfigDTO> GetAll()
        {
            //TODO pisar las config debug
            return GetAllFromDB();
        }


        private IDictionary<string, ConfigDTO> GetAllFromDB()
        {
            Dictionary<string, ConfigDTO> retVal = null;
            try
            {
                retVal = this.UoW.RepoCache<Entities.Config>().Find(c => c.ModuleId == this.ModuleId && c.Debug == false)
                                .Select(c => (ConfigDTO)c.Map())
                                .ToDictionary(c => c.Key);
                Logger.Debug(string.Format("Config: GetAll for ModuleId {0}", ModuleId));
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw new ServiceException("Couldn't get all configs from DB", ex);
            }

            return retVal;
        }
    }
}
