﻿namespace CA.Common.DBService.EntityAdministrators
{
    using Base;
    using Base.Exceptions;
    using CA.Common.Base.Entities;
    using CA.Common.Base.Service.Log;
    using CA.Common.Base.ServiceInterfaces;
    using CA.Common.Entities;
    using CA.Common.Exceptions;
    using CA.Common.IRepository;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public class GenericEntityAdministrator<TEntity, TId> : BaseDBService, IGenericEntityAdministrator<TEntity, TId>
        where TEntity : IEntity<TId>
    {                                
        private IGenericRepository<TEntity> repo;

        public GenericEntityAdministrator(ISvcParams flags, ILogger logger, IUOW uow, IConfigService config) : base(flags, logger, uow, config)
        {
            this.repo = uow.RepoCache<TEntity>();
        }

        public TEntity GetOne(TId id)
        {
            TEntity retVal = default(TEntity);
            try
            {
                var proxy = this.repo.Load(id);
                var prop = proxy.entity_created_at;
                retVal = proxy;
            }
            catch (NHibernate.ObjectNotFoundException) { }

            return retVal;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.repo.FindAll();
        }

        public IList<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filters)
        {
            return this.repo.Find(filters);
        }

        /// <summary>
        /// Inserta una nueva entidad
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>    
        public TEntity Save(TEntity entity)
        {
            this.Validate(entity);
            this.StampTrackFields(entity);
            return this.repo.Save(entity);
        }

        /// <summary>
        /// Actualiza una entidad existente
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>  
        public TEntity Update(TId id, TEntity entity)
        {
            TEntity retVal = default(TEntity);
            try
            {
                this.Validate(entity);
                this.StampTrackFields(entity);
                retVal = this.repo.Update(id, entity);
            }
            catch (NHibernate.ObjectNotFoundException ex)
            {
                throw new EntityNotFoundException(ex);
            }

            return retVal;
        }

        /// <summary>
        /// Inserta o actualiza una entidad, de acuerdo a su propiedad id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>  
        public TEntity SaveOrUpdate(TEntity entity)
        {
            TEntity retVal = default(TEntity);
            try
            {
                this.Validate(entity);
                this.StampTrackFields(entity);
                retVal = this.repo.SaveOrUpdate(entity);
            }
            catch (NHibernate.ObjectNotFoundException ex)
            {
                throw new EntityNotFoundException(ex);
            }
            return retVal;
        }

        public void Delete(TId id)
        {
            try
            {
                this.repo.Delete(id);
            }
            catch (NHibernate.ObjectNotFoundException ex)
            {
                throw new EntityNotFoundException(ex);
            }  
        }

        public void Delete(TEntity entity)
        {
            try
            {
                this.repo.Delete(entity);
            }
            catch (NHibernate.ObjectNotFoundException ex)
            {
                throw new EntityNotFoundException(ex);
            }  
        }

        public int Count(Expression<Func<TEntity, bool>> filters)
        {
            return this.repo.Count();
        }

        public TEntity CreateInstance()
        {
            Type ty = typeof(TEntity);
            return (TEntity)Activator.CreateInstance(ty);
        }

        private void Validate(TEntity entity)
        {
            if (!entity.Validate())
            {
                throw new ValidationException(entity.ValidationErrors);
            }
        }

        private void StampTrackFields(TEntity entity)
        {
            if (entity.entity_created_at == null)
            {
                entity.entity_created_at = DateTime.Now;
            }
            entity.entity_updated_at = DateTime.Now;
            entity.entity_user = Constants.USER_ANONIMOUS;
        }   
    }
}
