﻿using NHibernate;

namespace CA.Common.DBService.NHbrnt
{
    public interface ISessionStorageContainer
    {
        ISession GetCurrentSession();
        void Store(ISession session);
    }
}