﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Collections.Generic;
using System.Reflection;

namespace CA.Common.DBService.NHbrnt
{
    public class SessionFactory
    {
        private static ISessionFactory _sessionFactory;

        static SessionFactory()
        {
            MappingAssemblies = new List<Assembly>();
        }

        public static IList<Assembly> MappingAssemblies { get; private set; }
        public static bool ShowSql { get; set; }

        private static void Init()
        {
            Configuration config = new Configuration();
            config.Configure();

            _sessionFactory = Fluently.Configure(config)
             .Mappings(AddMappings)
             // habilitar para generar schema
             //.ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true, true))
             .ExposeConfiguration(cfg => cfg.SetInterceptor(new SqlStatementInterceptor()))
             .BuildSessionFactory();
        }

        private static void AddMappings(MappingConfiguration mappingCfg)
        {
            mappingCfg.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly());
            foreach (Assembly mapAssembly in MappingAssemblies)
            {
                mappingCfg.FluentMappings.AddFromAssembly(mapAssembly);
            }

            mappingCfg.FluentMappings.Conventions.Add(FluentNHibernate.Conventions.Helpers.Table.Is(x => x.TableName.ToUpper()));
            mappingCfg.FluentMappings.Conventions.Add(FluentNHibernate.Conventions.Helpers.AutoImport.Never());
        }

        public static ISession GetCurrentSession()
        {
            ISessionStorageContainer sessionStorageContainer = SessionStorageFactory.GetStorageContainer();
            ISession currentSession = sessionStorageContainer.GetCurrentSession();

            if (currentSession == null)
            {
                currentSession = GetNewSession();
                sessionStorageContainer.Store(currentSession);
                System.Diagnostics.Trace.WriteLine("SessionFactory: New session");
            }
            return currentSession;
        }

        private static ISession GetNewSession()
        {
            return GetSessionFactory().OpenSession();
        }

        private static ISessionFactory GetSessionFactory()
        {
            if (_sessionFactory == null)
            {
                Init();
            }
            return _sessionFactory;
        }
    }
}
