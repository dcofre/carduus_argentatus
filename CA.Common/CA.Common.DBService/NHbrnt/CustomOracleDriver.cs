﻿//using NHibernate;
//using NHibernate.SqlTypes;
//using NHibernate.Driver;
//using Oracle.ManagedDataAccess.Client;

//namespace CA.Common.DBService.NHbrnt
//{
//    public class CustomOracleDriver : OracleManagedDriver
//    {
//        /// <summary>
//        /// Initializes the parameter.
//        /// </summary>
//        /// <param name="dbParam">The db param.
//        /// <param name="name">The name.
//        /// <param name="sqlType">Type of the SQL.
//        protected override void InitializeParameter(System.Data.IDbDataParameter dbParam, string name, global::NHibernate.SqlTypes.SqlType sqlType)
//        {
//            base.InitializeParameter(dbParam, name, sqlType);
//            if ((sqlType is StringClobSqlType))
//            {
//                // Custom Oracle driver para known bug de NHibernate https://nhibernate.jira.com/browse/NH-465
//                ((OracleParameter)dbParam).OracleDbType = OracleDbType.NClob;
//            }
//        }
//    }
//}
