﻿using System.Diagnostics;
using System.Web;

namespace CA.Common.DBService.NHbrnt
{
    public static class SessionStorageFactory
    {
        private static ISessionStorageContainer _nhSessionStorageContainer;

        public static ISessionStorageContainer GetStorageContainer()
        {
            if (_nhSessionStorageContainer == null)
            {
                if (HttpContext.Current == null)
                    _nhSessionStorageContainer = new ThreadSessionStorageContainer();
                else
                    _nhSessionStorageContainer = new HttpSessionContainer();
                Trace.WriteLine(string.Format("SessionStorageFactory: New {0}", _nhSessionStorageContainer.GetType ()));                
            }

            return _nhSessionStorageContainer;
        }
    }
}
