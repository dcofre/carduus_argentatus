using NHibernate;
using System.Diagnostics;

namespace CA.Common.DBService.NHbrnt
{
    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {    
            return sql;
        }
    }
}