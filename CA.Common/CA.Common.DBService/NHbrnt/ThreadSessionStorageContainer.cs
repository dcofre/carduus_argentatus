﻿using System.Collections;
using System.Threading;
using NHibernate;

namespace CA.Common.DBService.NHbrnt
{
    public class ThreadSessionStorageContainer : ISessionStorageContainer
    {
        private static readonly Hashtable _nhSessions = new Hashtable();

        public ISession GetCurrentSession()
        {
            ISession nhSession = null;

            //TODO: Check the better form to reuse some sessions.
            //if (_nhSessions.Contains(GetThreadName()))
            //    nhSession = (ISession)_nhSessions[GetThreadName()];

            return nhSession;
        }

        public void Store(ISession session)
        {
            if (_nhSessions.Contains(GetThreadName()))
                _nhSessions[GetThreadName()] = session;
            else
                _nhSessions.Add(GetThreadName(), session);
        }

        private static string GetThreadName()
        {
            return Thread.CurrentThread.ManagedThreadId.ToString();
        }
    }
}
