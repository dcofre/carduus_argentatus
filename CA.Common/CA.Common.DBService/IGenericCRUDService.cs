﻿namespace CA.Common.DBService
{
    using System.Collections.Generic;
    using CA.Common.DTOs;
    using CA.Common.Entities;

    public interface IGenericCRUDService<TDTO, TEntity, TId>
       where TDTO : IDTO
        where TEntity : IEntity<TId>
    {
        TDTO Read(TId id);
        IEnumerable<TDTO> ReadAll();
        TDTO Create(TDTO dto);
        TDTO Update(TId id, TDTO dto);
        void Delete(TId id);
    }
}