﻿using NHibernate;
using CA.Common.Base.Service.Log;
using CA.Common.DBService.NHbrnt;
using CA.Common.IRepository;
using System;
using System.Collections.Generic;
using CA.Common.Entities;
using CA.Common.Base.IRepository;

namespace CA.Common.DBService.Repository
{
    public class UOW : IUOW, IDisposable
    {
        private ILogger _logger;
        private const string LOG_PREFIX = "*UOW* ";
        private Dictionary<string, object> _repoCache;
        protected readonly ISession Session;

        /// <summary>
        /// Unit of work
        /// </summary>
        /// <param name="disablePersistance">Establece si al momento de hacer el Save() se hace o no el commit</param> 
        public UOW(ILogger logger, bool disablePersistance = false)
        {
            if (logger == null) throw new ArgumentNullException("logger");

            _logger = logger;
            this.DisablePersistence = disablePersistance;
            this.Session = SessionFactory.GetCurrentSession();
            SessionId = Guid.NewGuid();
            _repoCache = new Dictionary<string, object>();
            _logger.Trace(string.Format(LOG_PREFIX + "Initializing SessionId: {0}", SessionId));
        }

        private bool _disablePersistence;
        public bool DisablePersistence
        {
            get
            {
                if (_disablePersistence) _logger.Warn(LOG_PREFIX + "DisablePersistence=true! won't persist!");
                return _disablePersistence;
            }
            private set
            {
                _disablePersistence = value;
            }
        }

        public Guid SessionId { get; private set; }

        public IGenericRepository<TPersistable> RepoCache<TPersistable>()
            where TPersistable : IPersistable
        {
            IGenericRepository<TPersistable> retVal;
            string type = typeof(TPersistable).Name;
            if (_repoCache.ContainsKey(type))
            {
                retVal = _repoCache[type] as IGenericRepository<TPersistable>;
            }
            else
            {
                retVal = new GenericRepository<TPersistable>(this.Session);
                _repoCache.Add(type, retVal);
            }
            return retVal;
        }

        public void BeginTran()
        {
            try
            {
                this.Session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                _logger.Trace(LOG_PREFIX + "BeginTran Ok");
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                throw ex;
            }

        }

        public void CommitTran()
        {
            if (!this.DisablePersistence)
            {
                var s = this.Session;
                try
                {
                    if (IsTransactionUsable(s.Transaction))
                    {
                        s.Transaction.Commit();
                    }
                    _logger.Trace(LOG_PREFIX + "CommitTran Ok");
                }
                catch (Exception ex)
                {
                    _logger.Log(ex);
                    s.Transaction.Rollback();
                    throw ex;
                }
            }
        }

        public void RollbackTran()
        {
            if (!this.DisablePersistence)
            {
                var s = this.Session;
                try
                {
                    if (IsTransactionUsable(s.Transaction))
                    {
                        s.Transaction.Rollback();
                    }
                    _logger.Trace(LOG_PREFIX + "RollbackTran Ok");
                }
                catch (Exception ex)
                {
                    _logger.Log(ex);
                    throw ex;
                }
            }
        }

        public void Dispose()
        {
            try
            {
                var s = this.Session;
                if (s.IsOpen)
                {
                    s.Close();
                }
                s.Dispose();
                GC.SuppressFinalize(this);
                _logger.Trace(string.Format(LOG_PREFIX + "DISPOSE. SessionId: {0}", SessionId));
            }
            catch (Exception ex)
            {
                _logger.Log(ex);
                throw ex;
            }
        }


        private bool IsTransactionUsable(ITransaction tran)
        {
            var ret = false;

            if (tran != null)
            {
                if (tran.IsActive)
                {
                    ret = true;
                }
                else
                {
                    _logger.Warn(LOG_PREFIX + "ITransaction.Active is false!");
                }
            }
            else
            {
                _logger.Warn(LOG_PREFIX + "ITransaction is NULL!");
            }
            return ret;
        }

    }
}