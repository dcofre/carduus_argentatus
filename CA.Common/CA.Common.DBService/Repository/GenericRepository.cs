﻿namespace CA.Common.DBService.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using NHibernate;
    using NHibernate.Linq;
    using NHibernate.Criterion;
    using CA.Common.IRepository;
    using Base.IRepository;

    public class GenericRepository<TPersistble> : IGenericRepository<TPersistble> where TPersistble : IPersistable
    {
        protected readonly ISession Session;

        public GenericRepository(ISession session)
        {
            this.Session = session;
        }

        public TPersistble Load(object id)
        {
            return this.Session.Load<TPersistble>(id);
        }

        public TPersistble LoadAndLock(object id)
        {
            return this.Session.Load<TPersistble>(id, LockMode.Upgrade);
        }

        public IEnumerable<TPersistble> FindAll()
        {
            return this.Session.CreateCriteria(typeof(TPersistble)).List<TPersistble>() as IEnumerable<TPersistble>;
        }

        public int Count()
        {
            return this.Session.CreateCriteria(typeof(TPersistble))
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>();
        }

        public object Max(Expression<Func<TPersistble, object>> filter)
        {
            var r = this.Session.CreateCriteria(typeof(TPersistble))
                .SetProjection(Projections.Max<TPersistble>(filter))
                .UniqueResult();
            //var expression = (UnaryExpression)filter.Body;
            //var m = expression.Operand as MemberExpression;
            //var t  = m.Type;
            return r;
        }

        public IList<TPersistble> FindAll(int index, int count)
        {
            return this.Session.CreateCriteria(typeof(TPersistble))
                .SetFetchSize(count)
                .SetFirstResult(index * count)
                .SetMaxResults(count).List<TPersistble>();
        }

        public TPersistble Save(TPersistble persistableObject)
        {
            this.Session.Save(persistableObject);
            return persistableObject;
        }

        public TPersistble Update(object id, TPersistble persistableObject)
        {
            this.Session.Update(persistableObject, id);
            return persistableObject;
        }

        public TPersistble SaveOrUpdate(TPersistble persistableObject)
        {
            this.Session.SaveOrUpdate(persistableObject);
            return persistableObject;
        }

        public void Delete(object id)
        {
            var obj = this.Load(id);
            this.Delete(obj);
        }

        public void Delete(TPersistble persistableObject)
        {
            this.Session.Delete(persistableObject);
        }

        public virtual IList<TPersistble> Find(Expression<Func<TPersistble, bool>> filter)
        {
            var query = this.Session.Query<TPersistble>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public virtual IList<TPersistble> Find(Expression<Func<TPersistble, bool>> filter, Func<IQueryable<TPersistble>, IOrderedQueryable<TPersistble>> orderBy, int index, int count, out int total)
        {
            var query = this.Session.Query<TPersistble>();


            if (filter != null)
            {
                query = query.Where(filter);
            }

            total = query.Count();

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            var result = query.Skip((index - 1) * count)
                        .Take(count)
                        .ToList();

            return result;
        }

        public void Flush()
        {
            this.Session.Flush();
        }

        public void Forget(TPersistble persistableObject)
        {
            this.Session.Evict(persistableObject);
        }
    }
}
