﻿using System.Reflection;

namespace CA.Common.Web
{
    public class Reflector
    {
        public static Assembly GetAssembly()
        {  
            return System.Reflection.Assembly.GetExecutingAssembly();   
        }
    }
}
