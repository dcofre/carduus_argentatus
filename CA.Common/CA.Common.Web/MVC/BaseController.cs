﻿using CA.Common.Base.Service.Log;
using System.Web.Mvc;

namespace CA.Common.Web.MVC
{
    public abstract class BaseController : Controller
    {
        protected readonly ILogger logger;

        public BaseController(ILogger logger) : base()
        {
            this.logger = logger;
        }
    }
}
