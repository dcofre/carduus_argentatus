﻿namespace CA.Common.Web.WebApi
{
    using Base.Exceptions;
    using CA.Common.Base.Service.Log;
    using Exceptions;
    using System;
    using System.Web.Http;

    public abstract class BaseApiController : ApiController
    {
        protected readonly ILogger logger;

        public BaseApiController(ILogger logger) : base()
        {
            this.logger = logger;
        }

        protected IHttpActionResult ManageException(Exception ex)
        {
            IHttpActionResult retVal = InternalServerError();
            if (ex is ValidationException || ex is ArgumentException || ex is ArgumentNullException)
            {
                retVal = BadRequest();
            }
            else if (ex is EntityNotFoundException)
            {
                retVal = NotFound();
            }

            //400 Bad Request -The request is malformed, such as message body format error.
            //401 Unauthorized - Wrong or no authencation ID/ password provided.
            //403 Forbidden - It's used when the authentication succeeded but authenticated user doesn't have permission to the request resource
            //404 Not Found -When a non-existent resource is requested
            //405 Method Not Allowed - The error checking for unexpected HTTP method.For example, the RestAPI is expecting HTTP GET, but HTTP PUT is used.
            //429 Too Many Requests - The error is used when there may be DOS attack detected or the request is rejected due to rate limiting

                return retVal;
        }
    }
}
