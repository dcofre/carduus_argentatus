﻿namespace CA.Common.Web.WebApi
{
    using CA.Common.DTOs;
    using CA.Common.Entities;
    using DBService;
    using System;
    using System.Web.Http;

    [Authorize]
    public abstract class GenericCRUDController<TDTO, TEntity, TId> : BaseApiController
        where TDTO : IDTO
        where TEntity : IEntity<TId>
    {
        protected IGenericCRUDService<TDTO, TEntity, TId> service;

        public GenericCRUDController(GenericCRUDService<TDTO, TEntity, TId> service) : base(service.Logger)
        {
            this.service = service;
        }

        // GET api/values
        //[AllowAnonymous]
        //  [Route("Register")]
        public IHttpActionResult Get()
        {
            try
            {
                var list = this.service.ReadAll();
                if (list.GetEnumerator().MoveNext())
                {
                    return Ok(list);
                }
                else
                {
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return (this.ManageException(ex));
            }
        }

        // GET api/values/5
        public IHttpActionResult Get(TId id)
        {
            try
            {
                TDTO dto = this.service.Read(id);

                if (dto == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(dto);
                }

            }
            catch (Exception ex)
            {
                return (this.ManageException(ex));
            }
        }

        // POST api/values
        [AcceptVerbs("POST")]
        public IHttpActionResult Post([FromBody]TDTO value)
        {
            try
            {
                return Ok(this.service.Create(value));
                // return CreatedAtRoute<TDTO>("", new { id = product.Id }, null);
            }
            catch (Exception ex)
            {
                return (this.ManageException(ex));
            }
        }

        // PUT api/values/5
        [AcceptVerbs("PUT")]
        public IHttpActionResult Put(TId id, [FromBody]TDTO value)
        {
            try
            {
                return Ok(this.service.Update(id, value));
            }
            catch (Exception ex)
            {
                return (this.ManageException(ex));
            }
        }

        // DELETE api/values/5
        [AcceptVerbs("DELETE")]
        public IHttpActionResult Delete(TId id)
        {
            try
            {
                this.service.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return (this.ManageException(ex));
            }
        }
    }
}
