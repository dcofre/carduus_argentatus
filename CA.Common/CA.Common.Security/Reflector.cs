﻿using System.Reflection;

namespace CA.Common.Security
{
    public class Reflector
    {
        public static Assembly GetAssembly()
        {  
            return System.Reflection.Assembly.GetExecutingAssembly();   
        }
    }
}
