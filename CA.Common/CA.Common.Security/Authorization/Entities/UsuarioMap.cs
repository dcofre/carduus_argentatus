﻿using CA.Common.Security.Entities;
using FluentNHibernate.Mapping;

namespace CA.Common.Security.Usuarios
{
    public class UsuarioMap : ClassMap<Usuario>
    {
        public UsuarioMap()
        {
            Table("SEG_Usuario");

            Id(x => x.id)
                .GeneratedBy.Identity();

            Map(x => x.nombre)
                .Length(100)
                .Not.Nullable();

            Map(x => x.apellido)
                .Length(100)
                .Nullable();

            Map(x => x.usuario)
                .Length(50)
                .Not.Nullable();

            Map(x => x.password)
              .Length(50)
              .Not.Nullable();
        }
    }
}
