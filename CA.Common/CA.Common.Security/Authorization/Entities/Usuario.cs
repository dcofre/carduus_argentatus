﻿using CA.Common.Entities;
using System;

namespace CA.Common.Security.Entities
{
    public class Usuario : BaseEntity<int>
    {
        public Usuario()
        {
        }

        public virtual System.String nombre { get; set; }
        public virtual System.String apellido { get; set; }
        public virtual System.String mail { get; set; }
        public virtual System.String direccion { get; set; }
        public virtual System.String telefono { get; set; }
        public virtual System.String cel { get; set; }
        public virtual System.Int32 idcuenta { get; set; }

        public virtual System.String usuario { get; set; }
        public virtual System.String password { get; set; }
        public virtual System.Boolean habilitado { get; set; }
        public virtual System.Int32 mailsasignados { get; set; }

        public virtual System.Boolean aceptabarrido { get; set; }
        //  public virtual System.Boolean aceptarebotado { get; set; }

        public virtual String idcultura { get; set; }
        public virtual System.Int32 cantidadarecibir { get; set; }
        public virtual System.Int32 cantidadrecibida { get; set; }

        public virtual System.Int32 tipovendedor { get; set; }

        public virtual DateTime? ultimologin { get; set; }
        public virtual System.Boolean accesoexterno { get; set; }
        //public virtual System.Int32 idacceso { get; set; }
        //public virtual Acceso Acceso { get; set; }
        public virtual System.String passcorreo { get; set; }
        public virtual System.String nrointerno { get; set; }

        public virtual System.String horario_ingreso { get; set; }
        public virtual System.String horario_salida { get; set; }

        //public virtual Cuenta Cuenta { get; set; }
        //public virtual IList<Perfil> Perfiles { get; set; }

        public override bool Validate()
        {
            //Todo Validaciones
            return true;
        }
    }
}
