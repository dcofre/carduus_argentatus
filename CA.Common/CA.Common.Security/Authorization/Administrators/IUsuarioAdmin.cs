﻿using CA.Common.Base.ServiceInterfaces;
using CA.Common.Security.Entities;

namespace CA.Common.Security.Administrators
{
    public interface IUsuarioAdmin: IDBService, IBaseService
    {
        Usuario GetUsuario(string usuario);
        Usuario GetUsuario(int id);
    }
}