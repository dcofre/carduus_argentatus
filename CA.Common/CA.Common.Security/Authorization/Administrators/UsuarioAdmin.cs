﻿using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.DBService.EntityAdministrators;
using CA.Common.IRepository;
using CA.Common.Security.Entities;
using System;
using System.Linq;

namespace CA.Common.Security.Administrators
{
    public class UsuarioAdmin : GenericEntityAdministrator<Usuario,int>, IUsuarioAdmin
    {
        public UsuarioAdmin(ISvcParams pars, ILogger logger, IUOW uow, IConfigService config) : base(pars, logger, uow, config)
        { }

        /// <summary>
        /// Valida usuario por numero de legajo y si esta activo.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        //public bool IsValid(Usuario usuario)
        //{
        //    var r = true;

        //    if (usuario == null)
        //    {
        //        return false;
        //    }

        //    if (usuario.Legajo == 0 && String.IsNullOrEmpty(usuario.Login))
        //    {
        //        return false;
        //    }

        //    if (String.IsNullOrEmpty(usuario.Login))
        //    {
        //        var users = this.GetByFilter(x => x.Legajo == usuario.Legajo);
        //        r = users.Any(x => x.Activo);
        //    }
        //    else
        //    {
        //        var users = this.GetByFilter(x => x.Login == usuario.Login);
        //        r = users.Any(x => x.Activo);
        //    }

        //    return r;
        //}


        public Usuario GetUsuario(string usuario)
        {
            Usuario retorno = null;

            try
            {
                var tempLst = this.GetByFilter(usr => usr.usuario == usuario);

                if (tempLst.Any())
                {
                    retorno = tempLst.Single();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retorno;
        }


        public Usuario GetUsuario(int id)
        {
            Usuario retorno = null;

            try
            {
                var tempLst = this.GetByFilter(usr => usr.id == id);

                if (tempLst.Any())
                {
                    retorno = tempLst.Single();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retorno;
        }
    }
}
