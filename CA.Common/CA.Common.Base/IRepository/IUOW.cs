﻿namespace CA.Common.IRepository
{
    using Base.IRepository;
    using System;

    public interface IUOW
    {
        IGenericRepository<TPersistable> RepoCache<TPersistable>() where TPersistable : IPersistable;
        Guid SessionId { get; } //Testing  purpose
        void CommitTran();
        void BeginTran();
        void Dispose();
        void RollbackTran();
    }
}
