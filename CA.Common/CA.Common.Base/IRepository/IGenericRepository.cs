﻿namespace CA.Common.IRepository
{
    using CA.Common.Base.IRepository;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// Repositorio de objetos persistibles
    /// </summary>
    /// <typeparam name="TPersistable"></typeparam>
    public interface IGenericRepository<TPersistable> where TPersistable : IPersistable
    {
        IList<TPersistable> Find(Expression<Func<TPersistable, bool>> filter);
        IList<TPersistable> Find(Expression<Func<TPersistable, bool>> filter, Func<IQueryable<TPersistable>, IOrderedQueryable<TPersistable>> orderBy, int index, int count, out int total);
        TPersistable Load(object id);
        TPersistable LoadAndLock(object id);
        IEnumerable<TPersistable> FindAll();
        IList<TPersistable> FindAll(int index, int count);
        int Count();
        object Max(Expression<Func<TPersistable, object>> filter);
        TPersistable Update(object id, TPersistable persistableObject);
        TPersistable SaveOrUpdate(TPersistable persistableObject);
        TPersistable Save(TPersistable persistableObject);
        void Delete(object id);
        void Delete(TPersistable persistableObject);
        void Forget(TPersistable persistableObject);
    }
}
