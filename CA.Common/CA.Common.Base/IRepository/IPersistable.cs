﻿namespace CA.Common.Base.IRepository
{
    /// <summary>
    /// Representa un objeto persistible en un repositorio
    /// </summary>
    public interface IPersistable
    {
    }
}
