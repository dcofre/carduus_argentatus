﻿using System.Collections.Generic;

namespace CA.Common.Validation
{
    public abstract class ValidatableValidatorBase : IValidatableValidator
    {
        public void Validate(IValidatable value)
        {
            try
            {
                value.IsValid = ExecuteValidation(value);

                if (!value.IsValid && value.ValidationErrors == null || value.ValidationErrors.Count == 0)
                {
                    AddError(value, "El valor es inválido");
                }
            }
            catch (System.Exception ex)
            {
                AddError(value, string.Format("Error al validar el valor {0}: {1}", value, ex.Message));
            }
        }

        protected void AddError(IValidatable value, string errDescription)
        {
            if (value.ValidationErrors == null)
            {
                value.ValidationErrors = new List<string>();
            }

            value.ValidationErrors.Add(errDescription);
            value.IsValid = false;
        }


        protected abstract bool ExecuteValidation(IValidatable value);

    }
}
