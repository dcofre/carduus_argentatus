﻿namespace CA.Common.Validation
{
    public interface IGenericValidator<T>
    {
        bool Validate(T value);
    }
}
