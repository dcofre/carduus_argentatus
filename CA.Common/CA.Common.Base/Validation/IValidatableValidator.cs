﻿namespace CA.Common.Validation
{
    public interface IValidatableValidator
    {
        void Validate(IValidatable value);
    }
}
