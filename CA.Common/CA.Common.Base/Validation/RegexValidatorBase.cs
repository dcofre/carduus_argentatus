﻿using System;
using System.Text.RegularExpressions;

namespace CA.Common.Validation
{
    public abstract class RegexValidatorBase : IRegexValidator 
    {              
        public abstract string Regex { get; }

        public virtual bool Validate(string value)
        {
            bool retVal;

            try
            {
                var reg = new Regex(this.Regex);
                retVal = reg.IsMatch(value);
            }
            catch (Exception)
            {
                retVal = false;
            }

            return retVal;
        }
    }
}
