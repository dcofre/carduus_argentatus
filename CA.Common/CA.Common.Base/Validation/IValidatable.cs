﻿using System.Collections.Generic;

namespace CA.Common.Validation
{
    public interface IValidatable
    {
        bool Validate();
        IList<string> ValidationErrors { get; set; }
        bool IsValid { get; set; }
    }
}
