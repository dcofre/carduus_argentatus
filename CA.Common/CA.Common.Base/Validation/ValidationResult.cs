﻿using System;
using System.Collections.Generic;

namespace CA.Common.Validation
{
    public class ValidationResult : IValidatable
    {
        public ValidationResult()
        {
            this.ValidationErrors = new List<string>();
        }

        public ValidationResult(IValidatable source) : this()
        {
            this.IsValid = source.IsValid;
            this.ValidationErrors = source.ValidationErrors;
        }

        public bool IsValid
        {
            get
            {
                return (ValidationErrors.Count == 0);
            }
            set
            {
                //interfaz
            }
        }

        private IList<string> _validationErrors;
        public IList<string> ValidationErrors
        {
            get
            {
                return _validationErrors;
            }

            set
            {
                _validationErrors = value;
            }
        }

        public bool Validate()
        {
            throw new NotImplementedException();
        }
    }
}
