﻿namespace CA.Common.Validation
{
    public interface IRegexValidator: IGenericValidator <string>
    {
        string Regex { get;  }
    }
}
