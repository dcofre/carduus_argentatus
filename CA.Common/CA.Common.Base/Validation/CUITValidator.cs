﻿namespace CA.Common.Validation
{
    public class CUITValidator : RegexValidatorBase
    {
        public override string Regex
        {
            get
            {
                return @"^\d{9,11}$";
            }
        }
    }
}
