﻿using CA.Common.Base.Service.Log;

namespace CA.Common.Base.ServiceInterfaces
{
    public interface IInitializable
    {
        void Init(ILogger logger, IConfigService config);
    }
}