﻿using CA.Common.Base.Service.Log;

namespace CA.Common.Base.ServiceInterfaces
{
    public interface IBaseService
    {
        ILogger Logger { get; set; }
        ISvcParams Pars { get; set; }
    }
}