﻿using CA.Common.Base.Service.Log;
using CA.Common.IRepository;

namespace CA.Common.Base.ServiceInterfaces
{
    public interface IDBService
    {
        IUOW UoW { get; set; }
        IConfigService Config { get; set; }
    }
}