﻿using CA.Common.DTOs;
using System;
using System.Collections.Generic;


namespace CA.Common.Base.ServiceInterfaces
{
    public interface IConfigService
    {
        string ModuleId { get; }
        /// <summary>
        /// Obtiene el registro de configuración correspondiente a la key
        /// </summary>
        /// <returns></returns>
        ConfigDTO Get(string key);
        /// <summary>
        /// Obtiene el registro de configuración correspondiente a la key y al módulo especificado.
        /// </summary>
        /// <returns></returns>
        ConfigDTO Get(string key, string moduleId);
        /// <summary>
        /// Obtiene el listado de registros de configuracion
        /// </summary>
        /// <returns></returns>
        IDictionary<string, ConfigDTO> GetAll();

        ConfigDTO Upsert(ConfigDTO dto);
    }
}
