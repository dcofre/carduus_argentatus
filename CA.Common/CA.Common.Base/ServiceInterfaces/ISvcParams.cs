﻿namespace CA.Common.Base.ServiceInterfaces
{
    public interface ISvcParams
    {
        bool DebugMode { get; set; }
    }
}