﻿namespace CA.Common.Base.ServiceInterfaces
{
    public interface IAppVerService
    {
        string DateToShow { get; }
        string Version { get; }
        string VersionId { get; }
    }
}