﻿using System;
using System.Collections.Generic;

namespace CA.Common.Base.Process
{
    public class ProcessResult
    {
        public ProcessResult()
        {
            this.ExecDate = DateTime.Now;
        }

        public DateTime ExecDate { get; set; }
        public IList<ProcessOneResult> ProcessList { get; set; }
    }
}

