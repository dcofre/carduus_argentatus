﻿using System.Collections.Generic;

namespace CA.Common.Base.Process
{
    public interface IProcessor
    {
        string Id { get; }
        IList<IProcessable> GetProcessBatch(ProcessResult lastProcess, IProcessable processingUnit);
        ProcessOneResult ProcessOne(IProcessable entity);
    }
}
