﻿using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using System.Collections.Generic;

namespace CA.Common.Base.Process
{
    public abstract class ProcessorBase : IProcessor
    {
        protected ILogger _logger;
        protected IConfigService _config;

        public ProcessorBase(ILogger logger, IConfigService config)
        {
            _logger = logger;
            _config = config;
        }    

        public abstract string Id { get; }
        public abstract IList<IProcessable> GetProcessBatch(ProcessResult lastProcess, IProcessable processingUnit);
        public abstract ProcessOneResult ProcessOne(IProcessable entity);
    }
}
