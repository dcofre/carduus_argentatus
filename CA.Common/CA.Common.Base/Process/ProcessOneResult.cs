﻿using System;
using System.Collections.Specialized;

namespace CA.Common.Base.Process
{
    public class ProcessOneResult
    {
        public ProcessOneResult()
        {
            this.Vars = new StringDictionary();
            this.ExecDate = DateTime.Now;
        }

        public DateTime ExecDate { get; set; }
        public IProcessable Entity { get; set; }
        public bool Success { get; set; }
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public StringDictionary Vars { get; set; }
    }
}
