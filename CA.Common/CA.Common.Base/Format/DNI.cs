﻿using System;
using System.Text.RegularExpressions;

namespace CA.Common.Format
{
    public class DNI : IFormatProvider, ICustomFormatter
    {

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }
            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg == null ||  string.IsNullOrEmpty(arg.ToString()) || arg.ToString() == "0")
            {
                return string.Empty;
            }

            var validatePattern = @"^\d{6,8}$";

            try
            {
                var toFormat = arg.ToString();


                var reg = new Regex(validatePattern);
                if (!reg.IsMatch(toFormat))
                {
                    return string.Empty;
                }


                string result;

                var last = toFormat.Substring(toFormat.Length - 3);
                string middle;

                if (toFormat.Length <= 6)
                {
                    middle = toFormat.Substring(0, toFormat.Length - 3);
                    result = string.Format("{0}.{1}", middle, last);
                }
                else
                {
                    middle = toFormat.Substring(toFormat.Length - 6, 3);
                    var first = toFormat.Substring(0, toFormat.Length - 6);
                    result = string.Format("{0}.{1}.{2}", first, middle, last);
                }

                return result;

            }
            catch (FormatException e)
            {
                throw new FormatException(string.Format("The format of '{0}' is invalid.", format), e);
            }
        }
    }
}
