﻿using System.Net.Sockets;
using System.Text;

namespace CA.Common.Base.Clients
{
    /// <summary>
    /// State object for reading client data asynchronously
    /// </summary>
    public class StateObject
    {
        public const int BufferSize = 1024;

        public StateObject()
        {
            WorkSocket = null;
            Buffer = new byte[BufferSize];
            Sb = new StringBuilder();
        }

        public Socket WorkSocket { get; set; }
        public byte[] Buffer { get; set; }
        public StringBuilder Sb { get; set; }
    }
}
