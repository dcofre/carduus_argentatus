﻿using System;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using CA.Common.Base.Service;

namespace CA.Common.Base.Clients
{
    public class ClientAsyncBase
    {
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ManualResetEvent sendDone = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);

        public delegate void NotifyChangeHandler(string msg);
        public static event NotifyChangeHandler OnNotifyChange;
        public delegate void NotifyErrorHandler(Exception ex);
        public static event NotifyErrorHandler OnNotifyError;

        private String response = String.Empty;

        protected string targetIP;
        protected int targetPort;

        protected ClientAsyncBase(string ip, int port)
        {
            targetIP = ip;
            targetPort = port;
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;

                client.EndConnect(ar);

                NotifyChange(String.Format("Socket connected to {0}", client.RemoteEndPoint.ToString()));

                connectDone.Set();
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }
        }

        private void Receive(Socket client)
        {
            try
            {
                StateObject state = new StateObject();
                state.WorkSocket = client;

                client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.WorkSocket;

                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

                    client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    if (state.Sb.Length > 1)
                    {
                        response = state.Sb.ToString();
                    }
                    receiveDone.Set();
                }
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }
        }

        private void Send(Socket client, string data)
        {
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] byteData = encoder.GetBytes(data + "<EOF>");

            client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;

                int bytesSent = client.EndSend(ar);
                NotifyChange(String.Format("Sent {0} bytes to server.", bytesSent));

                sendDone.Set();
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }
        }

        protected string SendCommand(string cmd)
        {
            var res = string.Empty;
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {

                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(targetIP), targetPort);

                //client.BeginConnect(serverEndPoint, new AsyncCallback(ConnectCallback), client);
                //connectDone.WaitOne();

                client.Connect(serverEndPoint);

                Send(client, cmd);
                sendDone.WaitOne();

                Receive(client);
                receiveDone.WaitOne();

                if (response.IndexOf("<EOF>") != -1)
                {
                    res = response.Replace("<EOF>", "");
                    NotifyChange(String.Format("Response received : {0}", res));
                }
                else
                {
                    throw new Exception("El mensaje recibido no esta completo.");
                }

                client.Shutdown(SocketShutdown.Both);
                client.Close();

            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }

            return res;

        }

        /// <summary>
        /// Envia un mensaje de prueba con el texto del parametro. En caso de ser exitosa la comunicacion devuelve el mismo texto.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns>El mismo texto que recibio como paramatro.</returns>
        protected string TestMsg(string msg = "Test Dummy Message")
        {
            var response = string.Empty;

            try
            {
                var responseText = string.Empty;
                string jsonCmd = JsonService.Serializar(msg);
                responseText = SendCommand(jsonCmd);
                response = JsonService.Deserializar<string>(responseText);

            }
            catch (Exception ex)
            {
                response = String.Format("ERROR : {0}", ex.Message);
            }

            return response;

        }

        /// <summary>
        /// Envia un msg para notificar cambios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyChange(string msg)
        {
            if (OnNotifyChange != null)
            {
                OnNotifyChange(msg);
            }
        }

        /// <summary>
        /// Envia un msg para notificar errores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyError(Exception ex)
        {
            if (OnNotifyError != null)
            {
                OnNotifyError(ex);
            }
        }
    }
}
