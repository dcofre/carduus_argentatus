﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using CA.Common.Base.Service;

namespace CA.Common.Base.Clients
{
    public class ClientSyncBase
    {
        private ManualResetEvent connectDone = new ManualResetEvent(false);
        private ManualResetEvent sendDone = new ManualResetEvent(false);
        private ManualResetEvent receiveDone = new ManualResetEvent(false);

        public delegate void NotifyChangeHandler(string msg);
        public static event NotifyChangeHandler OnNotifyChange;
        public delegate void NotifyErrorHandler(Exception ex);
        public static event NotifyErrorHandler OnNotifyError;

        private String response = String.Empty;

        protected string targetIP;
        protected int targetPort;

        protected ClientSyncBase(string ip, int port)
        {
            targetIP = ip;
            targetPort = port;
        }

        protected bool CanConnect()
        {
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(targetIP), targetPort);
                client.Connect(serverEndPoint);

                if (client.Connected)
                {
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }

            return false;
        }

        protected string SendCommand(string cmd)
        {
            var res = string.Empty;
            byte[] bytes = new byte[StateObject.BufferSize];
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {

                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(targetIP), targetPort);

                client.Connect(serverEndPoint);

                NotifyChange(String.Format("Socket connected to {0}", client.RemoteEndPoint.ToString()));

                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] byteData = encoder.GetBytes(cmd + "<EOF>");
                int bytesSent = client.Send(byteData);
                NotifyChange("Waiting for response...");

                var response = String.Empty;

                using (var resultStream = new MemoryStream())
                {
                    byte[] buffer = new byte[8192];
                    int bytesReceived;
                    while ((bytesReceived = client.Receive(buffer, buffer.Length, SocketFlags.None)) > 0)
                    {
                        byte[] actual = new byte[bytesReceived];
                        Buffer.BlockCopy(buffer, 0, actual, 0, bytesReceived);
                        resultStream.Write(actual, 0, actual.Length);
                    }

                    response = Encoding.ASCII.GetString(resultStream.ToArray(), 0, resultStream.ToArray().Length);
                }

                if (response.IndexOf("<EOF>") != -1)
                {
                    res = response.Replace("<EOF>", "");
                    NotifyChange(String.Format("Response received : {0}", res));
                }
                else
                {
                    throw new Exception("El mensaje recibido no esta completo.");
                }

                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }

            return res;

        }

        private string ReadFromStream(NetworkStream stream)
        {
            var res = string.Empty;

            var messages = new List<byte[]>();
            while (stream.DataAvailable)
            {
                var message = new byte[1024];
                stream.Read(message, 0, 1024);

                messages.Add(message);
            }

            IEnumerable<byte> finalMessage = new byte[] { };

            foreach (var msg in messages)
                finalMessage = finalMessage.Concat(msg);

            if (finalMessage.Count() > 0)
                res = new ASCIIEncoding().GetString(finalMessage.ToArray(), 0, finalMessage.Count());

            return res;
        }

        /// <summary>
        /// Envia un mensaje de prueba con el texto del parametro. En caso de ser exitosa la comunicacion devuelve el mismo texto.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns>El mismo texto que recibio como paramatro.</returns>
        protected string TestMsg(string msg = "Test Dummy Message")
        {
            var response = string.Empty;

            try
            {
                var responseText = string.Empty;
                string jsonCmd = JsonService.Serializar(msg);
                responseText = SendCommand(jsonCmd);
                response = JsonService.Deserializar<string>(responseText);

            }
            catch (Exception ex)
            {
                response = String.Format("ERROR : {0}", ex.Message);
            }

            return response;

        }

        /// <summary>
        /// Envia un msg para notificar cambios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyChange(string msg)
        {
            if (OnNotifyChange != null)
            {
                OnNotifyChange(msg);
            }
        }

        /// <summary>
        /// Envia un msg para notificar errores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyError(Exception ex)
        {
            if (OnNotifyError != null)
            {
                OnNotifyError(ex);
            }
        }

        /// <summary>
        /// Obtiene la ip del equipo.
        /// </summary>
        /// <returns></returns>
        protected static string GetIP4Addresses()
        {
            var lst =  Dns.GetHostAddresses(Dns.GetHostName())
                .Where(IPA => IPA.AddressFamily == AddressFamily.InterNetwork)
                .Select(x => x.ToString());

            if (lst.Any()) 
            {
                return lst.First();
            }

            return "0.0.0.0";
            
        }
    }
}
