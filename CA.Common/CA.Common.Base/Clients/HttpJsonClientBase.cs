﻿using CA.Common.Base.Service;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CA.Common.Base.Clients
{
    public class HttpJsonClientBase
    {
        private double _secsToTimeout = 240;
        private readonly string _url;
        private readonly string _user;
        private readonly string _pass;


        public HttpJsonClientBase(string wsUrl)
        {
            ValidateUrl(wsUrl);
            this._url = wsUrl;
        }

        public HttpJsonClientBase(string wsUrl, string user, string pass)
        {
            ValidateUrl(wsUrl);

            if (String.IsNullOrEmpty(user))
            {
                throw new Exception("Debe indicarse una usuario válido para el cliente.");
            }
            if (String.IsNullOrEmpty(pass))
            {
                throw new Exception("Debe indicarse una contraseña válida para el cliente.");
            }

            this._url = wsUrl;
            this._user = user;
            this._pass = pass;
        }

        private void ValidateUrl(string wsUrl)
        {
            if (String.IsNullOrEmpty(wsUrl))
            {
                throw new Exception("Debe indicarse una URL válida para el cliente.");
            }
        }

        public HttpClient CreateClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this._url);
            client.DefaultRequestHeaders.Authorization = GetHeaderAuthorization(this._user, this._pass);
            client.Timeout = TimeSpan.FromSeconds(_secsToTimeout);
            return client;
        }

        private AuthenticationHeaderValue GetHeaderAuthorization(string user, string pwd)
        {
            if (user == null || pwd == null || user.Length < 1 || pwd.Length < 1)
            {
                return null;
            }
            else
            {
                var scheme = "basic";
                var param = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", user, pwd)));
                var header = new AuthenticationHeaderValue(scheme, param);
                return header;
            }
        }

        /// <summary>
        /// Método POST.
        /// </summary>
        /// <param name="metodo">Nombre del método (URLBUI/Método).</param>
        /// <param name="data">Datos a transmitir.</param>
        /// <returns>HttpResponseMessage de la transacción.</returns>
        public async Task<HttpResponseMessage> Post(string metodo, object data)
        {
            HttpResponseMessage retorno = null;

            ValidateMetodo(metodo);

            var request = DataToJson(data);

            var url = String.Format("{0}{1}", this._url, metodo);

            using (HttpClient httpClient = CreateClient())
            {
                try
                {
                    var response = httpClient.PostAsync(url, request).Result;

                    if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        throw new Exception("La API devolvió una respuesta vacía");
                    }

                    retorno = response;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return retorno;
        }


        public Paquete PostPaquete(string qry, Paquete data)
        {
            Paquete retVal = null;
            data.Parametros = JsonService.Serializar(data.Parametros);

            var response = Post(qry, data).Result;

            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                retVal = JsonService.Deserializar<Paquete>(content);
            }
            else
            {
                CheckResponseErrors("PostPaquete", response);
            }

            return retVal;
        }

        private StringContent DataToJson(object data)
        {
            var jsonData = JsonService.Serializar(data);
            return new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");
        }

        /// <summary>
        /// Método GET a la API BUI.
        /// </summary>
        /// <param name="metodo">Método a consultar (URLBUI/método).</param>
        /// <returns>HttpResponseMessage de la transacción.</returns>

        public string Get(string metodo)
        {
            return Get<string>(metodo);
        }

        public T Get<T>(string metodo)
        {
            T res;
            ValidateMetodo(metodo);
            var url = String.Format("{0}{1}", this._url, metodo);
            using (var httpClient = CreateClient())
            {
                try
                {
                    var response = httpClient.GetStringAsync(url);
                    res = JsonService.Deserializar<T>(response.Result);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return res;
        }

        public byte[] GetBytes(string metodo)
        {
            byte[] res = null;

            ValidateMetodo(metodo);

            var url = String.Format("{0}{1}", this._url, metodo);

            using (var httpClient = CreateClient())
            {
                try
                {                    
                    var response = httpClient.GetAsync(url);
                    if (response.Result.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        throw new Exception("El servicio devolvió un valor inválido");
                    }
                    var respString = response.Result.Content.ReadAsStringAsync();
                    res = JsonService.Deserializar<byte[]>(respString.Result);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return res;
        }

        private void ValidateMetodo(string metodo)
        {
            if (String.IsNullOrEmpty(metodo))
            {
                throw new Exception("El método rest no puede estar vacío");
            }
        }

        public void CheckResponseErrors(string methodName, HttpResponseMessage response)
        {
            string errMsg = string.Empty;
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    var result = response.Content.ReadAsStringAsync().Result;

                    var error = JsonService.Deserializar<dynamic>(result);
                    try
                    {
                        errMsg = error.Message;
                    }
                    catch
                    {
                        errMsg = string.Format("Ocurrió un error al invocar el método {0} y no se pudo obtener información sobre el mismo", methodName);
                    }
                }
                else
                {
                    errMsg = string.Format("El método {0} devolvió una respuesta vacía", methodName);
                }
            }

            if (errMsg != String.Empty)
            {
                throw new Exception(errMsg);
            }
        }

        public double SecondsToTimeout
        {
            get
            {
                return _secsToTimeout;
            }
            set
            {
                _secsToTimeout = value;
            }
        }

    }
}

