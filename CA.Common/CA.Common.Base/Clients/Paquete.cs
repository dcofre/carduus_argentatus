﻿using CA.Common.Base.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CA.Common.Base.Clients
{
    public class Paquete
    {
        public Paquete()
        {
            Errores = new List<string>();
        }

        public Paquete(dynamic parametros, string command = null, Cabecera cabecera = null) : this()
        {
            this.Command = command;
            this.Cabecera = cabecera;
            this.Parametros = parametros;
        }

        public string Command { get; set; }
        public Cabecera Cabecera { get; set; }
        public dynamic Parametros { get; set; }
        public List<string> Errores { get; set; }

        public bool EsValido
        {
            get { return !Errores.Any(); }
        }

        public Tdto CommandToDto<Tdto>()
        {
            if (!String.IsNullOrEmpty(this.Command))
            {
                return JsonService.Deserializar<Tdto>(this.Command);
            }
            else
            {
                return default(Tdto);
            }
        }
    }

    public class Cabecera
    {
        public int NroPos { get; set; }
        public string Ip { get; set; }
        public string Hash { get; set; }
    }
}
