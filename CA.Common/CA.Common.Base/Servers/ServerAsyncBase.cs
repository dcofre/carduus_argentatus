﻿using CA.Common.Base.Clients;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CA.Common.Servers
{
    /// <summary>
    /// Clase base para armar un server asincronico.
    /// </summary>
    /// <remarks>
    /// No realiaza ninguna operacion especifica. 
    /// Los cambios y errores los notifica a traves de los eventos OnNotifyChange v OnNotifyError respectivamente.
    /// </remarks>
    public class ServerAsyncBase
    {
        private static readonly ManualResetEvent allDone = new ManualResetEvent(false);

        private static int Port { get; set; }

        public delegate void NotifyChangeHandler(string msg);
        public static event NotifyChangeHandler OnNotifyChange;
        public delegate void NotifyErrorHandler(Exception ex);
        public static event NotifyErrorHandler OnNotifyError;

        public delegate void ReadedCallbackHandler(object sender, string cmd);
        public static event ReadedCallbackHandler OnReadedCallback;

        public ServerAsyncBase()
        {
        }

        protected static void StartListening(int port)
        {
            Port = port;
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Port);

            NotifyChange(String.Format("Start Listening on {0}:{1}", ipAddress, port));

            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset();

                    NotifyChange("Waiting for a connection...");
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                NotifyError(new Exception("Fail on Start Listening", e));
            }
        }

        public static void StopListening()
        {
            NotifyChange("Stop Listening");
            Environment.Exit(0);
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            allDone.Set();

            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            StateObject state = new StateObject();
            state.WorkSocket = handler;
            handler.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.WorkSocket;

            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

                content = state.Sb.ToString();
                if (content.IndexOf("<EOF>") > -1)
                {
                    content = content.Replace("<EOF>", "");
                    NotifyChange(String.Format("Read {0} bytes from socket. \n Data : {1}", content.Length, content));
                    ReadedCallback(handler, content);
                }
                else
                {
                    handler.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
                }
            }
        }

        protected static void Send(Socket handler, dynamic data)
        {
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] byteData = encoder.GetBytes(data + "<EOF>");
            handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSent = handler.EndSend(ar);
                NotifyChange(String.Format("Sent {0} bytes to client.", bytesSent));

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception e)
            {
                NotifyError(new Exception("Fail when sending response.", e));
            }
        }

        /// <summary>
        /// Notifica al servicio "hijo" que ha finalizado de leer la respuesta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        private static void ReadedCallback(object sender, string cmd)
        {
            if (OnReadedCallback != null)
            {
                OnReadedCallback(sender, cmd);
            }
        }

        /// <summary>
        /// Envia un msg para notificar cambios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyChange(string msg)
        {
            if (OnNotifyChange != null)
            {
                OnNotifyChange(msg);
            }
        }

        /// <summary>
        /// Envia un msg para notificar errores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyError(Exception ex)
        {
            if (OnNotifyError != null)
            {
                OnNotifyError(ex);
            }
        }
    }
}