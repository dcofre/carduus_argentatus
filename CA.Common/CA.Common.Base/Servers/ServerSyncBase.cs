﻿using CA.Common.Base.Clients;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CA.Common.Servers
{
    /// <summary>
    /// Clase base para armar un server sincronico.
    /// </summary>
    /// <remarks>
    /// No realiaza ninguna operacion especifica. 
    /// Los cambios y errores los notifica a traves de los eventos OnNotifyChange y OnNotifyError respectivamente.
    /// </remarks>
    public class ServerSyncBase
    {
        private static readonly ManualResetEvent allDone = new ManualResetEvent(false);

        private static int Port { get; set; }

        public delegate void NotifyChangeHandler(string msg);
        public static event NotifyChangeHandler OnNotifyChange;
        public delegate void NotifyErrorHandler(Exception ex);
        public static event NotifyErrorHandler OnNotifyError;

        public delegate void ReadedCallbackHandler(object sender, string cmd);
        public static event ReadedCallbackHandler OnReadedCallback;

        public static string data = null;

        public ServerSyncBase()
        {
        }

        protected static void StartListening(int port)
        {
            Port = port;
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Port);

            NotifyChange(String.Format("Start Listening on {0}:{1}", ipAddress, port));

            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            byte[] bytes = new byte[StateObject.BufferSize];

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    NotifyChange("Waiting for a connection...");

                    Socket handler = listener.Accept();
                    data = null;

                    while (true)
                    {
                        try
                        {
                            bytes = new byte[StateObject.BufferSize];
                            int bytesRec = handler.Receive(bytes);
                            if (bytesRec == 0)
                            {
                                break;
                            }
                            data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                            if (data.IndexOf("<EOF>") > -1)
                            {
                                data = data.Replace("<EOF>", "");
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            NotifyError(new Exception("Fail getting data", ex));
                            handler.Shutdown(SocketShutdown.Both);
                            handler = listener.Accept();
                            break;
                        }
                       
                    }

                    NotifyChange(String.Format("Text received : {0}", data));

                    ReadedCallback(handler, data);
                }
            }
            catch (Exception e)
            {
                NotifyError(new Exception("Fail on Start Listening", e));
            }
        }

        public static void StopListening()
        {
            NotifyChange("Stop Listening");
            Environment.Exit(0);
        }


        protected static void Send(Socket handler, dynamic data)
        {
            ASCIIEncoding encoder = new ASCIIEncoding();
            try
            {
                byte[] byteData = encoder.GetBytes(data + "<EOF>");
                handler.Send(byteData);
                NotifyChange(String.Format("Sent {0} bytes to client.", byteData.Length));
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception ex)
            {
                NotifyError(ex);
            }
          
        }

        /// <summary>
        /// Notifica al servicio "hijo" que ha finalizado de leer la respuesta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        private static void ReadedCallback(object sender, string cmd)
        {
            if (OnReadedCallback != null)
            {
                OnReadedCallback(sender, cmd);
            }
        }

        /// <summary>
        /// Envia un msg para notificar cambios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyChange(string msg)
        {
            if (OnNotifyChange != null)
            {
                OnNotifyChange(msg);
            }
        }

        /// <summary>
        /// Envia un msg para notificar errores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="cmd"></param>
        protected static void NotifyError(Exception ex)
        {
            if (OnNotifyError != null)
            {
                OnNotifyError(ex);
            }
        }
    }
}