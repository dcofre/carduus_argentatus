﻿namespace CA.Common.Base.Notification
{
    public interface INotifier
    {
        string Id { get; }
        NotificationResult Notify(INotifiable entity, bool forceNotification);
    }
}
