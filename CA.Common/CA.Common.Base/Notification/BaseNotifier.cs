﻿using System;
using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;

namespace CA.Common.Base.Notification
{
    public abstract class BaseNotifier : INotifier
    {
        protected IConfigService _config;
        protected ILogger _logger;

        public BaseNotifier(IConfigService config, ILogger logger)
        {
            _config = config;
            _logger = logger;
        }

        public abstract string Id { get; }

        public abstract NotificationResult Notify(INotifiable entity, bool forceNotification);

    }
}
