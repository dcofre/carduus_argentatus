﻿namespace CA.Common.Base.Notification
{
    public interface IGenericNotifier<T> : INotifier where T : INotifiable
    {
         NotificationResult Notify(T entity);

    }
}