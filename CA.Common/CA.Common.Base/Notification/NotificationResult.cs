﻿using System;

namespace CA.Common.Base.Notification
{
    public class NotificationResult
    {
        public bool Success { get; set; }
        public DateTime ExecutionDate { get; set; }
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public string NotifierKey { get; set; }
    }
}
