﻿using System;

namespace CA.Common.Base.Service.Log
{
    public interface ILogger
    {
        void Trace(string message);
        void Warn(string message);
        void Error(string message);
        void Debug(string message);
        void Info(string message);
        void Log(string message, Exception ex);
        void Log(Exception ex);
    }
}
