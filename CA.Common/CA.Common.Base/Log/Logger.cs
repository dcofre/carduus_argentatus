﻿using NLog;
using System;
using System.Text;

namespace CA.Common.Base.Service.Log
{
    public class Logger : ILogger
    {
        private readonly NLog.Logger logger;

        public Logger(string loggerName)
        {
            logger = LogManager.GetLogger(loggerName);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Debug(string message)
        {
            logger.Debug (message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Log(Exception ex)
        {
            Log(null, ex);
        }

        public void Log(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            if (message !=null) sb.AppendLine(String.Format("Context message: {0}", message));
            sb.AppendLine(String.Format("Error Type: {0}", ex.GetType().FullName));
            sb.AppendLine(String.Format("Error Message: {0}", ex.Message));
            if (ex.InnerException != null)
            {
                sb.AppendLine(String.Format("Inner Message: {0}", ex.InnerException.Message));
            }
            sb.AppendLine(String.Format("Error Stack Trace: {0}", ex.StackTrace));

            logger.Error(sb.ToString());
        }
    }
}
