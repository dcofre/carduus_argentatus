﻿using System;
using System.Diagnostics;
using System.Text;


namespace CA.Common.Base.Service.Log
{
    public class EventLogger : ILogger
    {
        private string loggerName;
        private readonly string logName = "Application";

        public EventLogger(string loggerName)
        {
            this.loggerName = loggerName;
        }

        public void Trace(string message)
        {
            message = CheckSource(message);
            EventLog.WriteEntry(loggerName, message, EventLogEntryType.Information);
        }

        public void Warn(string message)
        {
            message = CheckSource(message);
            EventLog.WriteEntry(loggerName, message, EventLogEntryType.Warning);
        }

        public void Error(string message)
        {
            message = CheckSource(message);
            EventLog.WriteEntry(loggerName, message, EventLogEntryType.Error);
        }


        public void Debug(string message)
        {
            message = CheckSource(message);
            EventLog.WriteEntry(loggerName, message, EventLogEntryType.Information);
        }

        public void Info(string message)
        {
            message = CheckSource(message);
            EventLog.WriteEntry(loggerName, message, EventLogEntryType.Information);
        }


        public void Log(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            if (message != null && message.Length > 0  )
            {
                sb.AppendLine(string.Format("Context Message: {0}", message));
            }
            sb.AppendLine(string.Format("Error Type: {0}", ex.GetType().FullName));
            sb.AppendLine(string.Format("Error Message: {0}", ex.Message));
            if (ex.InnerException != null)
            {
                sb.AppendLine(string.Format("Inner Message: {0}", ex.InnerException.Message));
            }
            sb.AppendLine(string.Format("Error Stack Trace: {0}", ex.StackTrace));

            var ckMsg = CheckSource(sb.ToString());
            EventLog.WriteEntry(loggerName, ckMsg, EventLogEntryType.Error);
        }

        public void Log(Exception ex)
        {
            Log(null, ex);
        }

        private string CheckSource(string msg)
        {
            if (!EventLog.SourceExists(loggerName))
            {
                EventLog.CreateEventSource(loggerName, logName);
            }

            //Log entry string is too long. A string written to the event log cannot exceed 32766 characters.
            var r = msg;
            if (msg.Length > 30000)
            {
                r = msg.Substring(0, 30000) + "-EVENTLOG TRUNCADO INTENCIONALMENTE-";
            }

            return r;
        }
    }
}
