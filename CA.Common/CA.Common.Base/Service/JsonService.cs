﻿using System;
using Newtonsoft.Json;

namespace CA.Common.Base.Service
{
    public static class JsonService
    {
        public static JsonSerializerSettings GetOps(bool ignoreNull)
        {
            var ops = new Newtonsoft.Json.JsonSerializerSettings();
            if (ignoreNull)
            {
                ops.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                ops.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
                ops.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
            }
            ops.Converters.Add(new Newtonsoft.Json.Converters.IsoDateTimeConverter());

            return ops;
        }

        public static string Serializar(dynamic cmd)
        {
            string jsonCommand = JsonConvert.SerializeObject(cmd, Formatting.None, GetOps(false));
            return jsonCommand;
        }

        public static T Deserializar<T>(string command)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(command);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("No fue posible deserializar. El comando no tiene un formato correcto", ex);
            }
        }

        public static T Deserializar<T>(string command, bool ignoreNull)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(command, GetOps(ignoreNull));
            }
            catch
            {
                throw new InvalidOperationException("No fue posible deserializar. El comando no tiene un formato correcto");
            }
        }

        /*###################  2013/08/06 - protobuf-net ###################
        * Integramos la libreria protobuf-net(2.0.0.640) e hicimos algunas pruebas, comprobamos que tiene mejor performance que Newtonsoft.Json
        * Decidimos no hacer el cambio ahora, ya que protobuf-net requiere decorar las clases a serializar/deserealizar.
        ################### ###################*/
    }
}
