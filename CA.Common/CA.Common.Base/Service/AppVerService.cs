﻿namespace CA.Common.Base.Service
{
    using ServiceInterfaces;
    using global::System;
    using global::System.Diagnostics;
    using global::System.IO;
    using global::System.Reflection;

    public class AppVerService : IAppVerService
    {
        private Assembly _verAssembly;
        private FileVersionInfo _verInfo;

        public AppVerService()
        {
           Init( Assembly.GetCallingAssembly());
        }

        public AppVerService(Assembly verAssembly)
        {
            if (verAssembly == null)
            {
                throw new ArgumentNullException("verAssembly");
            }
            Init( verAssembly);
        }
        

        private void Init(Assembly verAssembly)
        {
            _verAssembly = verAssembly;
            _verInfo = FileVersionInfo.GetVersionInfo(verAssembly.Location);
        }

        /// <summary>
        /// Devuelve un codigo único para la version de la aplicación
        /// </summary>
        public string VersionId
        {
            get
            {
                return Date.Ticks.ToString();
            }
        }

        /// <summary>
        /// Devuelve la version de la aplicación
        /// </summary>
        public string Version
        {
            get
            {
                var debugMode = false;

#if DEBUG
                debugMode =  true;
#endif

                return string.Format("{0}{1}", _verAssembly.GetName().Version, (debugMode ? " DEBUG" : string.Empty));
            }
        }

        /// <summary>
        /// Fecha de Build de la app, formateada para el usuario
        /// </summary>
        public string DateToShow
        {
            get
            {
                return Date.ToString("dd/MM/yyyy HH:mm");
            }
        }

        /// <summary>
        /// Fecha de Build de la app
        /// </summary>
        public DateTime Date
        {
            get
            {
                return File.GetLastWriteTime(_verAssembly.Location);
            }
        }

        /// <summary>
        /// Nombre de producto
        /// </summary>
        public string ProductName
        {
            get
            {
                return _verInfo.ProductName  ;
            }
        }

        /// <summary>
        /// Nombre de producto
        /// </summary>
        public string GetCompanyName(string key)
        {
            return _verInfo.CompanyName;
        }

    }
}
