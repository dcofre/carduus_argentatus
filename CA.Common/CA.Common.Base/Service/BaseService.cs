﻿using System;
using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.Exceptions;
using CA.Common.Base.Exceptions;

namespace CA.Common.Base.Service
{
    public class BaseService : IBaseService
    {
        public BaseService(ISvcParams pars, ILogger logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }
            if (pars == null)
            {
                throw new ArgumentNullException("pars");
            }

            this.Logger = logger;
            this.Pars = pars;
        }

        public ILogger Logger { get; set; }
        public ISvcParams Pars { get; set; }

        protected void ServiceTryCatch(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
        }

        protected void ManageException(Exception ex)
        {
            if (ex is ValidationException || ex is EntityNotFoundException)
            {
                throw ex;
            }
            else if (ex is ServiceException)
            {
                this.Logger.Log(ex);
                throw ex;
            }
            else
            {
                var svcEx = new ServiceException("Excepción genérica del servicio", ex);
                this.Logger.Log(svcEx);
                throw svcEx;
            } 
        }

    }
}
