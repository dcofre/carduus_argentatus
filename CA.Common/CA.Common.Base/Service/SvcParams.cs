﻿using CA.Common.Base.ServiceInterfaces;

namespace CA.Common.Base.Service
{
    public class SvcParams : ISvcParams
    {
        public SvcParams(bool debugMode)
        {
            this.DebugMode = debugMode;
        }

        public SvcParams()
        {

#if DEBUG
            this.DebugMode = true;
#endif
        }

        public bool DebugMode
        { get; set; }
    }
}
