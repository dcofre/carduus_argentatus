﻿using CA.Common.Base.Service;
using CA.Common.Base.Service.Log;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace CA.Common.Base.Utils
{
    public static class Tracer
    {
        public static void TraceMethod(ILogger logger, Object[] parameters)
        {
            if (logger == null) return;
            StringBuilder strBuild = new StringBuilder();

            try
            {
                StackTrace st = new StackTrace(false);
                StackFrame sf = st.GetFrame(1);
                MethodBase method = sf.GetMethod();
                strBuild.AppendLine(string.Format("METHOD: {0}.{1}", method.ReflectedType, method.Name));

                if (parameters == null)
                {
                    strBuild.AppendLine("\tNO PARAMETERS TO TRACE");
                }
                else
                {
                    foreach (var obj in parameters)
                    {
                        if (obj == null)
                        {
                            strBuild.AppendLine("\tPARAMETER: Null");
                        }
                        else
                        {
                            strBuild.AppendLine(string.Format("\tPARAMETER: {0}", obj.GetType().ToString()));
                            strBuild.AppendLine(string.Format("\t\t{0}", JsonService.Serializar(obj)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }

            logger.Trace(strBuild.ToString());
        }
    }
}
