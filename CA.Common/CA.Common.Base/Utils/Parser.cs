﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA.Common.Base.Utils
{
    public static class Parser
    {
        public static Guid? ParseGuid(string id)
        {
            Guid? retVal = null;
            try
            {
                retVal = new Guid(id);
            }
            catch
            {
                //no loguea a proposito
            }
            return retVal;
        }
    }
}
