﻿namespace CA.Common.Base.Utils
{
    public class Container<T>
    {
        public Container(T value)
        {
            this.Value = value;
        }
        public T Value { get; set; }
    }
}
