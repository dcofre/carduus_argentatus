﻿using CA.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CA.Common.Base.Utils
{
    public static class Mapper
    {
        public static string ListToString(IList<string> list)
        {
            System.Text.StringBuilder strBuild = new StringBuilder();
            foreach (string str in list)
            {
                strBuild.AppendLine(str);
            }
            return strBuild.ToString();
        }
    }
}
