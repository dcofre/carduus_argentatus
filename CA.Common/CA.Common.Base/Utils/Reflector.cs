﻿using System.Reflection;

namespace CA.Common.Base.Utils
{
    public class Reflector
    {
        protected Reflector()
        { }

        private static Reflector _singleton;
        public static Reflector Get()
        {
            if (_singleton == null)
            {
                _singleton = new Reflector();
            }
            return _singleton;
        }

        public virtual Assembly Assembly
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly();
            }
        }

    }
}
