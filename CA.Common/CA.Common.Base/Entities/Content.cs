﻿using System;
using CA.Common.DTOs;

namespace CA.Common.Entities
{
    public class Content : BaseEntity<int>, IEntity<int>
    {
        public Content() { }

        public Content(IExchangeable dto) : this()
        {
            ContentDTO tr = (ContentDTO)dto;
            this.ModuleId = tr.ModuleId;
            this.Key = tr.Key;
            this.Value = tr.Value;
        }

        public virtual string ModuleId { get; set; }
        public virtual string Key { get; set; }
        public virtual string Value { get; set; }

        public virtual IExchangeable Map()
        {
            var ret = new ContentDTO();
            ret.ModuleId = this.ModuleId;
            ret.Key = this.Key;
            ret.Value = this.Value;
            return (IExchangeable)ret;
        }
    }
}

