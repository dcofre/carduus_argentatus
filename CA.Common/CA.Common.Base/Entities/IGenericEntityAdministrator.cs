﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CA.Common.Entities;

namespace CA.Common.Base.Entities
{
    public interface IGenericEntityAdministrator<TEntity, TId>
        where TEntity : IEntity<TId>
    {
        int Count(Expression<Func<TEntity, bool>> filters);
        void Delete(TId id);
        void Delete(TEntity entity);
        TEntity CreateInstance();
        IEnumerable<TEntity> GetAll();
        TEntity GetOne(TId id);
        IList<TEntity> GetByFilter(Expression<Func<TEntity, bool>> filters);
        TEntity Save(TEntity entity);
        TEntity Update(TId id, TEntity entity);
        TEntity SaveOrUpdate(TEntity entity);
    }
}