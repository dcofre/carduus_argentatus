﻿namespace CA.Common.Entities
{
    using System;
    using CA.Common.DTOs;
    using CA.Common.Base.IRepository;

    public class Config : BaseEntity<int>, IPersistable
    {
        public Config() { }

        public Config(IExchangeable dto) : this()
        {
            ConfigDTO cfg = (ConfigDTO)dto;
            this.ModuleId = cfg.ModuleId;
            this.Key = cfg.Key;
            this.Value = cfg.Value;
            this.Description = cfg.Description;
        }

        public virtual string ModuleId { get; set; }
        public virtual string Key { get; set; }
        public virtual bool Debug { get; set; }
        public virtual string Value { get; set; }
        public virtual string Description { get; set; }

        public virtual IExchangeable Map()
        {
            var ret = new ConfigDTO();
            ret.ModuleId = this.ModuleId;
            ret.Key = this.Key;
            ret.Value = this.Value;
            ret.Description = this.Description;
            return (IExchangeable)ret;
        }
    }
}

