﻿using System;
using System.Collections.Generic;

namespace CA.Common.Entities
{
    [Serializable()]
    public abstract class BaseEntity<TId> : IEntity<TId>
    {
        private IList<string> _validationErrors;

        protected BaseEntity()
        {
            _validationErrors = new List<string>();
        }

        public virtual IList<string> ValidationErrors
        {
            get
            {
                return _validationErrors;
            }
            set
            {
                _validationErrors = value;
            }
        }

        //dejar virtual para mockeo
        public virtual bool IsValid
        {
            get
            {
                return (_validationErrors != null && _validationErrors.Count > 0);
            }
            set { }
        }

        public virtual bool Validate()
        {
            return this.IsValid;
        }

        public virtual TId id { get; set; }
        public virtual DateTime? entity_created_at { get; set; }
        public virtual DateTime? entity_updated_at { get; set; }
        public virtual DateTime? entity_deleted_at { get; set; }
        public virtual string entity_user { get; set; }
    }
}
