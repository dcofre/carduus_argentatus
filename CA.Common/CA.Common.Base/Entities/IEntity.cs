﻿using CA.Common.Base.IRepository;
using CA.Common.Validation;
using System;

namespace CA.Common.Entities
{
    /// <summary>
    /// Representa una entidad persistible y trackeable
    /// </summary>
    public interface IEntity<TId> : IValidatable, IPersistable
    {
        TId id { get; set; }
        DateTime? entity_created_at { get; set; }
        DateTime? entity_updated_at { get; set; }
        DateTime? entity_deleted_at { get; set; }
        string entity_user { get; set; }
    }
}
