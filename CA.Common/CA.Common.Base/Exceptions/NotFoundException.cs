﻿using CA.Common.Exceptions;
using System;

namespace CA.Common.Exceptions
{
    [Serializable()]
    public class NotFoundException : ClientException
    {
        public NotFoundException(string msg = null, Exception innerEx = null)
            : base(404, msg, innerEx)
        { }
    }
}
