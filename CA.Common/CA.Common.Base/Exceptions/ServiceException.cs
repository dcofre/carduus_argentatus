﻿using System;
using System.Collections.Generic;

namespace CA.Common.Exceptions
{
    [Serializable]
    public class ServiceException : Exception
    {
        public ServiceException(string msg = null, Exception innerEx = null)
            : base(msg, innerEx)
        {
            this.Errors = new List<string>();
            if (!String.IsNullOrEmpty(msg))
            {
                this.Errors.Add(msg);
            }
        }

        public ServiceException(IList<string> errors)
            : this()
        {
            this.Errors.AddRange(errors);
        }

        public List<string> Errors { get; set; }
    }
}
