﻿using System;
using System.Collections.Generic;

namespace CA.Common.Exceptions
{
    [Serializable]
    public class ValidationException : ServiceException
    {
        public ValidationException(IList<string> errors) : base(errors)
        { }     
    }
}
