﻿using System;

namespace CA.Common.Exceptions
{
    [Serializable]
    public class NotAuthorizedException : Exception
    {     
        public NotAuthorizedException(string msg = null)
            : base(msg)
        {
            if (msg == null) msg = "Usuario o password incorrectos";
        }

    }
}
