﻿namespace CA.Common.DTOs
{
    public class ConfigDTO 
    {
        public virtual string ModuleId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
