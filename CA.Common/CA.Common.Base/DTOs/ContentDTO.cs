﻿namespace CA.Common.DTOs
{
    public class ContentDTO
    {
        public virtual string ModuleId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
