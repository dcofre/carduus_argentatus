﻿namespace CA.Common.Entities
{
    public interface IMappeable
    {
        IExchangeable Map();
    }
}
