﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CA.Common.Base.Service;
using CA.Common.Base.Service.Log;
using CA.Common.Base.Utils;
using System;
using System.Reflection;

namespace CA.Common.Service.Test
{
    [TestClass]
    public class AppVerTests
    {
        [TestMethod]
        public void CanGetVersion()
        {
            var vSvc = new AppVerService(Assembly.GetExecutingAssembly());
            var date = vSvc.Date;
            var strDate = vSvc.DateToShow;
            var ver = vSvc.Version;
            var verId = vSvc.VersionId;

            Assert.IsTrue(date != DateTime.MinValue &
                strDate != null && strDate.Length > 0 &
                ver != null & ver.Length > 0 &
                verId != null & verId.Length > 0);

        }
    }
}
