﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CA.Common.Base.Service.Log;
using CA.Common.Base.Utils;

namespace CA.Common.Service.Test
{
    [TestClass]
    public class UtilsTests
    {
        [TestMethod]
        public void CanTrace()
        {
            ILogger logger = new Logger("MiLog");
            var cfg = new ConfigDTO() { Description = "Descripcion", Value = "Un valor", ModuleId = "el modulo", Key = "Clave" };
            Tracer.TraceMethod(logger, new object[] { cfg, null });
        }

        [TestMethod]
        public void CanTraceWitNoParameters()
        {
            ILogger logger = new Logger("MiLog");
            Tracer.TraceMethod(logger, null);
        }
    }
}
