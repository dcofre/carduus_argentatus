﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace CA.Common.Service.Test
{
    [TestClass]
    public class ValidationTests
    {
        [TestMethod]
        public void CanValidateEmail()
        {
            string testEmail = "juan.carlos@lapelusa-BLABLA.com.ar";

            var emailValidator = new CA.Common.Validation.EmailValidator();

            Assert.IsTrue(emailValidator.Validate (testEmail));

           // Assert.IsTrue(emailValidator.Validate(testEmail.ToUpper()));

            testEmail = "nada";

            Assert.IsFalse(emailValidator.Validate(testEmail));
        }
    }
}
