﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CA.Common.Format;

namespace CA.Common.Service.Test
{
    [TestClass]
    public class FormatTests
    {
        [TestMethod]
        public void CanGetDNIFormatg()
        {
            var name = "DNI";
            var fmt = FormatFactory.Get(name);
            Assert.IsTrue(fmt != null);
        }
        [TestMethod]
        public void CanGetCUITFormatg()
        {
            var name = "CUIT";
            var fmt = FormatFactory.Get(name);
            Assert.IsTrue(fmt != null);
        }

    }
}
