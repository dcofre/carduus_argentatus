﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CA.Common.Base.Service;
using CA.Common.Base.Service.Log;
using CA.Common.Base.ServiceInterfaces;
using CA.Common.DBService;
using CA.Common.DBService.Repository;
using CA.Common.IRepository;
using System.Linq;

namespace CA.Common.Service.Test
{
    [TestClass]
    public class DBServiceTests
    {
        private readonly IUOW _uow;
        private readonly ILogger _logger;
        private readonly ISvcParams _pars;

        private readonly IConfigService _cfgSvc;

        string _nullTestKey = "rflkm0%3rgfr";
        string _TestKey = "test";

        public DBServiceTests()
        {
            CA.Common.DBService.NHbrnt.SessionFactory.ShowSql = true;

            _logger = new Logger("CA.Common.Servie.Test");
            _uow = new UOW(_logger);
            _pars = new SvcParams() { DebugMode = true };

            _cfgSvc = new ConfigService(_pars, _logger, _uow);

            FillTestData();
        }

        public void FillTestData()
        {
            _uow.BeginTran();

            if (_cfgSvc.Get(_TestKey) == null)
            {
                _cfgSvc.Upsert(new DTOs.ConfigDTO() { Key = _TestKey, Value = _TestKey });
            }

            _uow.CommitTran();
        }

        [TestMethod]
        public void CanGetOneConfig()
        {
            var cfg = _cfgSvc.Get(_cfgSvc.GetAll().First().Key);
            Assert.IsTrue(cfg != null && cfg.Value.TrimEnd().Length > 0);
        }

        [TestMethod]
        public void CanNotGetPConfig()
        {
            var cfg = _cfgSvc.Get(_nullTestKey);
            Assert.IsTrue(cfg == null);
        }

        [TestMethod]
        public void CanGetAllConfigs()
        {
            var cfgDict = _cfgSvc.GetAll();
            Assert.IsTrue(cfgDict != null && cfgDict.Count > 0);
        }

        [TestMethod]
        public void CanGetContent()
        {
            _uow.BeginTran();
            var cont = _contentSvc.Get(_TestKey, "CA", "<b>Valor para test</b>");
            _uow.CommitTran();
            //_uow.Dispose();
            Assert.IsTrue(cont != null);
        }


    }
}
